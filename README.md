# GRATE : GRaph Analysis of TEM Images

## Software developed to analyse HRTEM images of thin polymer films
#####Developed at: Ganapathysubramanian group ([ISU](https://www.me.iastate.edu/bglab)) and Chabinyc group ([UCSB](https://engineering.ucsb.edu/~chabinyc_group/))

Developed by: 
[Balaji Sesha Sarath Pokuri](mailto:balajip@iastate.edu); 
[Kathryn O'Hara](mailto:kathryn.ohara.27@gmail.com).

GUI development: Jacob Stimes



### Features
* Rapid identification of ordered regions in HRTEM image
* Modular framework that allows extensions 
* GUI based framework for ease of use

### Dependencies
* For source code: MATLABŪ (minimum version: 2015b). All the external libraries that are used by the software are either available through MATLABŪ (>= 2015b)  or included in the package.

### How to download
**Source Code**: Source code written in MATLABŪ. Download from the repository or perform a `git clone https://bitbucket.org/baskargroup/grate` and run `grate_main.m`. 

### How to use it
A sample video is included in the repository, named `GRATE_sample_working_3_26_18.mp4`. 

### Funding Acknowledgements
We gratefully acknowledge funding from NSF via NSF-DMREF 1435587 grant "DMREF/Collaborative Research: Controlling Hierarchical Nanostructures in Conjugated Polymers".

### Citing

To cite GRATE, please use the following bibtex entry:

```
@article{pokuri2019grate,
  title={GRATE: A framework and software for GRaph based Analysis of Transmission Electron Microscopy images of polymer films},
  author={Pokuri, Balaji Sesha Sarath and Stimes, Jacob and O’Hara, Kathryn and Chabinyc, Michael L and Ganapathysubramanian, Baskar},
  journal={Computational Materials Science},
  volume={163},
  pages={1--10},
  year={2019},
  publisher={Elsevier}
}
```

### Feel free to raise issues and contribute to the Software.

##Contact:
Baskar Ganapathysubramanian

Mechanical Engineering

Iowa State University

baskarg@iastate.edu
