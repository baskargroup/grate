function [startx,starty] = calc_strmln_start_points(edge_length, d_spac, theta)
    %%
    % d : d-spacing
    % theta: orientation in the crystallite
    % edge_length : size of square to do return start points
    % x,y : coordinates of the line, with center at (0,0)
    %% initial line
    fac = 1.5;
    num_points = floor(edge_length / d_spac);
    x = linspace(0, edge_length + 2*d_spac , num_points*fac) - edge_length/2;
    y = 0 * x;
    %     plot(x,y, '--');
    %% rotate line by theta
    A = [cosd(theta), -sind(theta); sind(theta), cosd(theta)];
    orig_coord = [x;y];
    rot = A * orig_coord;
    x_rot = rot(1,:);
    y_rot = rot(2,:);
%     plot(x_rot, y_rot, '-*');
%     xlim([-edge_length,edge_length])
%     ylim([-edge_length,edge_length])
    %% return values
    startx = x_rot;
    starty = y_rot;
end