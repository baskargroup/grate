classdef process_image_results < handle
    
    %Subclass handle so data isn't copied between function calls
    
    properties
        
        level
        bwImage
        I_thinned
        cc
        s
        majAxis
        minAxis
        thetabar_all
        centroid_all
        aspectRatio
        aspRatioFlag
        L_sparse
        S
        C
        histData
        histData_indexed
        indx
        
    end
    
    methods
        
        function obj=process_image_results(level,...
                bwImage,...
                I_thinned,...
                cc,...
                s,...
                majAxis,...
                minAxis,...
                thetabar_all,...
                centroid_all,...
                aspectRatio,...
                aspRatioFlag,...
                L_sparse,...
                S,...
                C,...
                histData,...
                histData_indexed,...
                indx)
            
            obj.level = level;
            obj.bwImage = bwImage;
            obj.I_thinned = I_thinned;
            obj.cc = cc;
            obj.s = s;
            obj.majAxis = majAxis;
            obj.minAxis = minAxis;
            obj.thetabar_all = thetabar_all;
            obj.centroid_all = centroid_all;
            obj.aspectRatio = aspectRatio;
            obj.aspRatioFlag = aspRatioFlag;
            obj.L_sparse = L_sparse;
            obj.S = S;
            obj.C = C;
            obj.histData = histData;
            obj.histData_indexed = histData_indexed;
            obj.indx = indx;
        end
        
    end
    
end