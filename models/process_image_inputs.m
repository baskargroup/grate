classdef process_image_inputs
    
    properties
        pixelScale
        imageFileName
        imgThreshold 
        deltaR
        polymerFlexibility
        usingAdaptiveThreshold 
        adaptiveThreshold
        adaptiveThresholdWindowSize 
        usingAspectRatioThreshold
        aspectRatioThreshold
        pixelThreshold
    end
    
    methods
        function obj=process_image_inputs(pixelScale, imageFileName, imgThreshold,...
                deltaR, polymerFlexibility, usingAdaptiveThreshold,...
                adaptiveThreshold , adaptiveThresholdWindowSize,...
                usingAspectRatioThreshold, aspectRatioThreshold, pixelThreshold)
                
            obj.pixelScale = pixelScale;
            obj.imageFileName = imageFileName;
            obj.imgThreshold = imgThreshold;
            obj.deltaR = deltaR;
            obj.polymerFlexibility = polymerFlexibility;
            obj.usingAdaptiveThreshold = usingAdaptiveThreshold;
            obj.adaptiveThreshold = adaptiveThreshold;
            obj.adaptiveThresholdWindowSize = adaptiveThresholdWindowSize;
            obj.usingAspectRatioThreshold = usingAspectRatioThreshold;
            obj.aspectRatioThreshold = aspectRatioThreshold;
            obj.pixelThreshold = pixelThreshold;
            
        end
        
    end
    
    methods(Static)
        
        function obj=getDefaults()
            defaultScale = 4.2;
            defaultimageFileName = '';
            defaultDeltaR = utils.convertnm2Pix(defaultScale,1.5);
            defaultPolymerFlexibility = 30; %delta theta
            defaultImgThreshold = 0.50;
            defaultAdaptiveThresholdWindowSize = utils.convertnm2Pix(defaultScale,1.5);
            defaultAdaptiveThreshold = 0;
            defaultAspectRatioThreshold = 0.75;
            defaultAspectRatioPixelThreshold = utils.convertnm2Pix(defaultScale,1.75);
            
            obj = process_image_inputs(defaultScale, defaultimageFileName , defaultImgThreshold,...
                defaultDeltaR, defaultPolymerFlexibility, 0,...
                defaultAdaptiveThreshold , defaultAdaptiveThresholdWindowSize,...
                0, defaultAspectRatioThreshold, defaultAspectRatioPixelThreshold);
        end
    end
end
