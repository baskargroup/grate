classdef plot_options < handle 
   
    properties
        I_bg
        plotComponents
        plotGroups
        backgroundImage
        componentInfoToPlot
        groupInfoToPlot
        numGroupsToPlot
        plotWithNames
        plotHistograms
        plotGlobalFFT
        plotLocalFFT
    end
    
    methods
       
        function obj=plot_options(I_bg, plotComponents, plotGroups,...
             backgroundImage, componentInfoToPlot, groupInfoToPlot,...
             numGroupsToPlot, plotWithNames,...
             plotHistograms, plotGlobalFFT, plotLocalFFT)
         
            obj.I_bg = I_bg;
            obj.plotComponents = plotComponents;
            obj.plotGroups = plotGroups;
            obj.backgroundImage = backgroundImage;
            obj.componentInfoToPlot = componentInfoToPlot;
            obj.groupInfoToPlot = groupInfoToPlot;
            obj.numGroupsToPlot = numGroupsToPlot;
            obj.plotWithNames = plotWithNames;
            obj.plotHistograms = plotHistograms;
            obj.plotGlobalFFT = plotGlobalFFT;
            obj.plotLocalFFT = plotLocalFFT;
         
        end
        
    end
end