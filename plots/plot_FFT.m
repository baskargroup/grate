function global_tree=plot_FFT(image, imageProcessingResults, fftOriginalImageAxes, fftGroupAxes, isGlobalDecomp)
axes(fftOriginalImageAxes);
cla
imshow(image,[],'InitialMagnification','fit');
hold off

%[r, c, ~] = size(image);
axes(fftGroupAxes);
cla
% imshow(ones(size(image)),[],'InitialMagnification','fit');
imshow(((image)),[],'InitialMagnification','fit');
hold off

if isGlobalDecomp
    % qtdecomp and grate outputs are decoupled.
    % newer tree implementation does not require splitting into multiple
    % sub-images
    handleToMessageBox = msgbox(strcat('The calculations are done assuming the "1" pixel per nm. Please scale the results accordingly.', ...
        'Please wait...'));
    
    global_tree = tree_structure_grate(image, fftGroupAxes);            
    if exist('handleToMessageBox', 'var')
        delete(handleToMessageBox);
        clear('handleToMessageBox');
    end
    
    
else
    %   Perform decomposition only for the identified 'domains' .. currently
    %   suppressed
    %   TODO : change this function to use new tree decomposition
    
    for group=1:imageProcessingResults.S
        
        groupCentroidX = mean(imageProcessingResults.centroid_all(1, (imageProcessingResults.C==group)));
        groupCentroidY = mean(imageProcessingResults.centroid_all(2, (imageProcessingResults.C==group)));
        
        groupMaxX = ceil(max(imageProcessingResults.centroid_all(1, (imageProcessingResults.C==group))));
        groupMaxY = ceil(max(imageProcessingResults.centroid_all(2, (imageProcessingResults.C==group))));
        
        groupMinX = floor(min(imageProcessingResults.centroid_all(1, (imageProcessingResults.C==group))));
        groupMinY = floor(min(imageProcessingResults.centroid_all(2, (imageProcessingResults.C==group))));
        
        subImage = image(groupMinX:groupMaxX, groupMinY:groupMaxY);
        [r, c, ~] = size(subImage);
        
        if r ~= c
            %Pad rectangular image into a square
            [newSubImage, rowsAdded, columnsAdded] = utils.pad_to_square(subImage);
            groupMaxX = groupMaxX + columnsAdded;
            groupMaxY = groupMaxY + rowsAdded;
            subImage = newSubImage;
        end
        
        [newSubImage, rowsAdded, columnsAdded] = utils.makePow2Image(subImage);
        groupMaxX = groupMaxX + columnsAdded;
        groupMaxY = groupMaxY + rowsAdded;
        subImage = newSubImage;
        
        [r, c, ~] = size(subImage);
        if group < 500 %TODO remove
            fft_data=qtdecomp_fft(image, subImage, fftGroupAxes, groupMinX, groupMinY);
            if isempty(fft_data)
                disp('empty fft_data');
            end
        else
            %disp('done');
            return;
        end
        
    end
    
end

    function start_pts = calc_image_patch_starts(wholedomain_size, n_patches, subdomain_size)
        n_patches = n_patches - 1;
        start_pts = zeros(n_patches,1);
        for ii = 1: n_patches-1
            start_pts(ii) = 1 + subdomain_size * (ii-1);
        end
        start_pts(n_patches) = wholedomain_size - subdomain_size;
        
    end
end

