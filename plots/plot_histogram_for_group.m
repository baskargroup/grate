function plot_histogram_for_group(groupToPlot, imageProcessingInputs, imageProcessingResults, plotOptions, plotAxes)
    % hObject    handle to button_plot3 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    ifhistAll = 0; %allGroupsCheckbox.Value; %get(handles.toggle_domains,'Value') == get(handles.toggle_domains,'Max');
    hist_domain_num = groupToPlot;%str2double(obj.domainNumberEdit.String);

    if isnan(hist_domain_num) && ifhistAll==0
      error('Please input the domain number to plot histogram');
    else
      ifplotAllHist = 0;
      ifPlotSpecificDomainHist = 0;
      if ifhistAll
        ifplotAllHist = 1;
        %disp('Plotting histograms of all domains');
      elseif ~isnan(hist_domain_num)
        ifPlotSpecificDomainHist = 1;
        %disp(sprintf('Plotting domain number %d', hist_domain_num));
      end
    end
    % plotting the histograms
    if (ifPlotSpecificDomainHist)
      angleTol = imageProcessingInputs.polymerFlexibility;
      % get the index of the domain to plot, from the sorted list of indices
      % according to population
      curr_index = (imageProcessingResults.indx(end-hist_domain_num+1));
      % get the coordinates of all the ellipsoids belonging to that domain
      x = imageProcessingResults.centroid_all(1,(imageProcessingResults.C==curr_index));
      y = imageProcessingResults.centroid_all(2,(imageProcessingResults.C==curr_index));
      % find the boundary points
      k = boundary(x',y',0.75);
      % find the ellipses whose centroid lies in the boundary
      [in,~] = inpolygon(imageProcessingResults.centroid_all(1,:),imageProcessingResults.centroid_all(2,:),x(k),y(k));
      axes(plotAxes);
      hist(imageProcessingResults.thetabar_all(in),45);
      title(sprintf('All components in \n Group rank # %d, mean orientation = %f ± %2.2f',hist_domain_num, mean(imageProcessingResults.thetabar_all(in)),angleTol));
      xlim([-90,90]);
      axis tight

      axes(plotAxes);
      hist(imageProcessingResults.thetabar_all(imageProcessingResults.C==curr_index),45);
      title(sprintf('Like oriented components in \n Group rank # %d, mean orientation = %f ± %2.2f',hist_domain_num, mean(imageProcessingResults.thetabar_all(imageProcessingResults.C==curr_index)),angleTol));
      xlim([-90,90]);
      axis tight

    end
end