function plot_group_info(imageProcessingInputs, imageProcessingResults, plotOptions, plotAxes)
%handles = setBackGround(handles);

% plots the domains on the background image
%handles.domainPlot = get(handles.popup_domain,'Value')-1;

%figure;
axes(plotAxes);
cla;
imshow(plotOptions.I_bg, 'InitialMagnification', 'fit');
% title('');
hold on;
%handles.numDomains = str2double(get(handles.text_domain,'String'));
%handles.ifNameDomains = get(handles.toggle_name,'Value') == get(handles.toggle_name,'Max');
numGroups = plotOptions.numGroupsToPlot;
domainPlot = plotOptions.groupInfoToPlot;
ifNameDomains = plotOptions.plotWithNames;
if (isnan(numGroups))
  error ('Enter the number of domains to plot');
else
  ifDrawDomains = 1;
  ifDrawAllEllipses = 0;
  ifDrawAllLines = 0 ;
  ifDrawLikeEllipses = 0;
  ifDrawLikeLines = 0;
  switch (domainPlot)
    case 1
      ifDrawDomains = 0;
      error('Select what feature of domains to plot');
    case 2
      % plot only domains
      ifDrawDomains = 1;
      disp('Plot Only Domains');
    case 3
      % plot domains with ellipsoids
      disp('Plot Domains with like oriented ellipsoids');
      ifDrawLikeEllipses = 1;
    case 4
      % plot domains with lines
      disp('Plot Domains with like oriented lines');
      ifDrawLikeLines = 1;
    case 5
      % plot domains with all ellipsoids
      disp('Plot Domains with all ellipsoids');
      ifDrawAllEllipses = 1;
    case 6
      % plot domains with all lines
      disp('Plot Domains with all lines');
      ifDrawAllLines = 1;
    case 7
      % plot the lines only
      disp('Plotting the like lines for the top domains');
      ifDrawDomains = 0;
      ifDrawLikeLines = 1;
    otherwise
      error ('Error in plotting domains');
  end
end

% screening based on aspect ratio threshold
usingAspectRatioThreshold = imageProcessingInputs.usingAspectRatioThreshold;
ifaspRatioThresh = imageProcessingInputs.aspectRatioThreshold;

% if (handles.usingAspectRatioThreshold && handles.ifaspRatioThresh)
%   s_plotting = s_plotting(handles.aspRatioFlag);
% end

% Plot subroutine
ax1 = gca;
hold on;
S_topX = numGroups;

% indices of the top 'X' populous domains
indx_top_X = (imageProcessingResults.histData_indexed (end-S_topX+1:end,1));
% title(sprintf('Largest %d groups', numGroups));

% iterate over the top 'X' domains
for jj = S_topX:-1:1
  curr_index = indx_top_X(jj);
  x = imageProcessingResults.centroid_all(1,(imageProcessingResults.C==curr_index));
  y = imageProcessingResults.centroid_all(2,(imageProcessingResults.C==curr_index));
  % find the boundary points
  k = boundary(x',y',0.75);
  t = cumsum(sqrt([0,diff(x(k))].^2 + [0,diff(y(k))].^2));
  x2 = spline(t,x(k),1:max(t));
  y2 = spline(t,y(k),1:max(t));
  if (ifDrawDomains)
%     plot(ax1, x2,y2,'LineWidth',3.0,'Color',[0.18 0 0.82]);hold all;
  end
  % find the ellipses whose centroid lies in the boundary
  if ifDrawAllEllipses || ifDrawAllLines
    [in,~] = inpolygon(imageProcessingResults.centroid_all(1,:),imageProcessingResults.centroid_all(2,:),x(k),y(k));
    if ifDrawAllLines
      % draw major axes for the all inside ellipses
      s_plotting = imageProcessingResults.s(in);
      if(usingAspectRatioThreshold && ifaspRatioThresh)
        s_plotting = imageProcessingResults.s(logical(in.*imageProcessingResults.aspRatioFlag));
      end
      drawEllipseMajorAxis(s_plotting,ax1,'LineWidth',1.5,'color','r');
      s_plotting = imageProcessingResults.s(imageProcessingResults.C==curr_index);
      if(usingAspectRatioThreshold && ifaspRatioThresh)
        s_plotting = imageProcessingResults.s(logical((imageProcessingResults.C==curr_index).*imageProcessingResults.aspRatioFlag));
      end
      if (plotOptions.backgroundImage == 5)
        drawEllipseMajorAxis(s_plotting,ax1,'LineWidth',1.5,'color','k');
      else
        drawEllipseMajorAxis(s_plotting,ax1,'LineWidth',1.5,'color','y');
      end
    elseif ifDrawAllEllipses
      s_plotting = imageProcessingResults.s(in);
      if(usingAspectRatioThreshold && ifaspRatioThresh)
        s_plotting = imageProcessingResults.s(logical(in.*imageProcessingResults.aspRatioFlag));
      end
      drawEllipses(s_plotting,ax1,'LineWidth',1.5,'color','r');
      s_plotting = imageProcessingResults.s(imageProcessingResults.C==curr_index);
      if(usingAspectRatioThreshold && ifaspRatioThresh)
        s_plotting = imageProcessingResults.s(logical((imageProcessingResults.C==curr_index).*imageProcessingResults.aspRatioFlag));
      end
      if (plotOptions.backgroundImage == 5)
        drawEllipses(s_plotting,ax1,'LineWidth',1.5,'color','k');
      else
        drawEllipses(s_plotting,ax1,'LineWidth',1.5,'color','y');
      end
    end
  elseif ifDrawLikeEllipses || ifDrawLikeLines
    s_plotting =  imageProcessingResults.s(imageProcessingResults.C==curr_index);
    if(usingAspectRatioThreshold && ifaspRatioThresh)
      s_plotting = imageProcessingResults.s(logical((imageProcessingResults.C==curr_index).*imageProcessingResults.aspRatioFlag));
    end
    if ifDrawLikeLines      
      if (plotOptions.backgroundImage == 5)
        drawEllipseMajorAxis(s_plotting,ax1,'LineWidth',1.5,'color','k');
      else
        drawEllipseMajorAxis(s_plotting,ax1,'LineWidth',1.5,'color','y');
      end
    elseif ifDrawLikeEllipses
      if (plotOptions.backgroundImage == 5)
        drawEllipses(s_plotting,ax1,'LineWidth',1.5,'color','k');
      else
      drawEllipses(s_plotting,ax1,'LineWidth',1.5,'color','y');
      end
    end
  end
  if (ifNameDomains)
    text(mean(x(k))-100,mean(y(k)),sprintf('Domain rank # %d',S_topX - jj +1),'Color','cyan','FontSize',10,'FontWeight','bold');
  end
  pause(0.5);
end
clear x y jj k in x2 y2 t ax1
end