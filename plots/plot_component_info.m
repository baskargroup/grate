function plot_component_info(imageProcessingInputs, imageProcessingResults, plotOptions, plotAxes)

% IF using this in production run, calling code needs to set plotAxes
% 'visible' to 'off' and save plot after function call

% Reset backgroundImage ?? -- should be automatically taken care of when the
% background image is changed by the user (I_bg is updated when changes are made
% to the pull down menu).
ifPlotOnlyEllipses = 0;
ifPlotOnlyLines = 0;
switch plotOptions.componentInfoToPlot
    case 1
        disp('Plotting only background image');
    case 2
        %disp('Plotting  lines on the background image');
        ifPlotOnlyEllipses = 0;
        ifPlotOnlyLines = 1;
    case 3
        %disp('Plotting ellipsoids on the background image');
        ifPlotOnlyEllipses = 1;
        ifPlotOnlyLines = 0;
    otherwise
        disp('Select right option for plotting on background image');
end

usingAspectRatioThreshold = imageProcessingInputs.usingAspectRatioThreshold;
usingAdaptiveThreshold = imageProcessingInputs.usingAdaptiveThreshold;

s_plotting = imageProcessingResults.s;
if (usingAspectRatioThreshold && usingAdaptiveThreshold)
    s_plotting = s_plotting(imageProcessingResults.aspRatioFlag);
end
if (ifPlotOnlyEllipses || ifPlotOnlyLines)
    axes(plotAxes);
    cla
    imshow(plotOptions.I_bg, 'InitialMagnification', 'fit');
    hold on;
    ax_curr = gca;
    if ifPlotOnlyEllipses
        if (plotOptions.backgroundImage == 5)
            drawEllipses(s_plotting,ax_curr,'LineWidth',1.5,'color','k');
        else
            drawEllipses(s_plotting,ax_curr,'LineWidth',1.5,'color','g');
        end
    elseif ifPlotOnlyLines
        if (plotOptions.backgroundImage == 5)
            drawEllipseMajorAxis(s_plotting,ax_curr,'LineWidth',1.5,'color','k');
        else
            drawEllipseMajorAxis(s_plotting,ax_curr,'LineWidth',1.5,'color','g');
        end
        drawnow();
    end
else
    axes(plotAxes);
    cla
    imshow(plotOptions.I_bg, 'InitialMagnification', 'fit');
    hold off;
    drawnow();
end
end