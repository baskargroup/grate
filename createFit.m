%% Copyright 2014-2016 Baskar Ganapathysubramanian
%% 
%% This file is part of GRATE.
%% 
%% GRATE is free software: you can redistribute it and/or modify it under the
%% terms of the GNU Lesser General Public License as published by the Free
%% Software Foundation, either version 2.1 of the License, or (at your option)
%% any later version.
%% 
%% GRATE is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
%% A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
%% details.
%% 
%% You should have received a copy of the GNU Lesser General Public License along
%% with GRATE.  If not, see <http://www.gnu.org/licenses/>.

% --- end license text --- %
function [fitresult, gof] = createFit(X, Y, F_filtered)
%CREATEFIT(X,Y,F_FILTERED)
%  Create a fit.
%
%  Data for 'untitled fit 1' fit:
%      X Input : X
%      Y Input : Y
%      Z Output: F_filtered
%  Output:
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%
%  See also FIT, CFIT, SFIT.


%% Fit: 'untitled fit 1'.
[xData, yData, zData] = prepareSurfaceData( X, Y, F_filtered );

% Set up fittype and options.
ft = fittype( 'a + c*exp(-((x.^2) /d1 + (y.^2)/d2 ))', 'independent', {'x', 'y'}, 'dependent', 'z' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [eps eps eps eps];
opts.StartPoint = [0.209976003894069 0.551194596112321 0.913375856139019 0.63235924622541];

% Fit model to data.
[fitresult, gof] = fit( [xData, yData], zData, ft, opts );

%%
% % Plot fit with data.
% figure( 'Name', 'untitled fit 1' );
% h = plot( fitresult, [xData, yData], zData );
% legend( h, 'untitled fit 1', 'F_filtered vs. X, Y', 'Location', 'NorthEast' );
% % Label axes
% xlabel X
% ylabel Y
% zlabel F_filtered
% grid on
%%



