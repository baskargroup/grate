function init_fft_tab(fftTab, image, imageProcessingResults, isGlobalDecomp)

global blur_status
global fftOriginalImageAxes

blur_status = false;
fftOriginalImageAxes = axes('Parent', fftTab,...
    'Units', 'Normalized',...
    'Position', [.05, .35, .4, .6],...
    'XTickMode', 'manual',...
    'XTick', [],...
    'XTickLabel', [],...
    'XColor', 'none',...
    'YTick', [],...
    'YTickLabel', [],...
    'YColor', 'none');

%This isn't working for some reason...
axes(fftOriginalImageAxes);
title('Original Image');

fftGroupAxes = axes('Parent', fftTab,...
    'Units', 'Normalized',...
    'Position', [.55, .35, .4, .6],...
    'XTickMode', 'manual',...
    'XTick', [],...
    'XTickLabel', [],...
    'XColor', 'none',...
    'YTick', [],...
    'YTickLabel', [],...
    'YColor', 'none');

statusLabel = uicontrol('Parent', fftTab,...
    'Units', 'normalized',...
    'Position', [.55, .2, .4, .1],...
    'Style', 'text',...
    'FontSize', 12,...
    'String', 'Computing decomposition...');


global_tree = plot_FFT(image, imageProcessingResults, fftOriginalImageAxes, ...
    fftGroupAxes, isGlobalDecomp);
plot_info_leaves(global_tree, image, fftGroupAxes)
statusLabel.String = '';

plot_blurred_button = uicontrol('Parent', fftTab,...
    'Units', 'Normalized',...
    'Position', [0.5, 0.15, 0.15, 0.05],...
    'Style', 'pushbutton',...
    'String', 'Blur/Unblur image',...
    'FontSize',10,...
    'Visible', 'off',...
    'Callback', {@plot_blurred_image, global_tree, image, fftGroupAxes});
plot_blurred_button.Visible = 'on';

save_tree_button = uicontrol('Parent', fftTab,...
    'Units', 'Normalized',...
    'Position', [0.67, 0.15, 0.15, 0.05],...
    'Style', 'pushbutton',...
    'String', 'Save Information',...
    'FontSize',10,...
    'Visible', 'off',...
    'Callback', {@save_tree, global_tree});
save_tree_button.Visible = 'on';

save_image_button = uicontrol('Parent', fftTab,...
    'Units', 'Normalized',...
    'Position', [0.85, 0.15, 0.1, 0.05],...
    'Style', 'pushbutton',...
    'String', 'Save Image',...
    'FontSize',10,...
    'Visible', 'off',...
    'Callback', {@save_image, fftGroupAxes});
save_image_button.Visible = 'on';
end

function plot_blurred_image(~,~, mytree, I, plotAxes)
%% blurs the leaves that do not have useful info
global blur_status
dt = mytree.depthtree;
dt1 = dt;
for i = 1:dt1.nnodes
    dt1 = dt1.set(i,mytree.get(i).noOfPeaks/2);
end
I_new = I;

c = mytree.findleaves;

for i=c
    if ~(dt1.get(i) > 0)    % leaves with no peaks
        d(1:4) = mytree.get(i).ranges;
        I_new(d(1):d(2), d(3):d(4)) = (I(d(1):d(2), d(3):d(4)).^2);
    end
end

axes(plotAxes);
if (~blur_status)
    imshow(I_new, [], 'InitialMagnification', 'fit');
    blur_status = true;
else
    plot_info_leaves(mytree, I, plotAxes);
    blur_status = false;
end
axis equal
end


function save_tree(~, ~, mytree)
%% Save the information in the leaves
pix_per_nm = 4.2;
dt = mytree.depthtree;
dt1 = dt;
for i = 1:dt1.nnodes
    dt1 = dt1.set(i,mytree.get(i).noOfPeaks/2);
end

c = mytree.findleaves;

% start writing into the file
fileID = fopen('FFT_info_qtree.txt','a+');
fprintf(fileID, 'location_x(pix),  location_y(pix),   Area(nm^2),   latt_spac,   latt_orient\n');

for i=c
    if (dt1.get(i) > 0)    % leaves with peaks
        try
            fft_info = reduce_data(mytree.get(i).fft_info);
            latt_spac = fft_info.latt_spac;
            latt_orient = fft_info.latt_orient;
            noOfPeaks = fft_info.noOfPeaks;
            ranges = mytree.get(i).ranges;
            % start writing for every peak observed
            for jj = 1:noOfPeaks
                if (latt_spac(jj) > 1.0) && (latt_spac(jj) < 5.)
                    fprintf(fileID, '%12.f,  %12.f,   %12.2f,   %12.2f,   %12.2f     \n', ...
                        0.5*(ranges(1)+ranges(2)), 0.5*(ranges(3) + ranges(4)), ...
                        (ranges(2)-ranges(1))*(ranges(4)-ranges(3))/(pix_per_nm^2), ...
                        latt_spac(jj), latt_orient(jj));
                end
            end
        catch
            disp(strcat('Error in writing information of leaf: ', num2str(i)));
        end
    end
end
fclose(fileID);
end

function plot_info_leaves(mytree, I, plotAxes)
%% plots the leaves which have fft information
dt = mytree.depthtree;
dt1 = dt;
for i = 1:dt1.nnodes
    dt1 = dt1.set(i,mytree.get(i).noOfPeaks/2);
end
colorOrder = ...
    [ 0 0 1 % 1 BLUE
    0 1 0 % 2 GREEN (pale)
    1 0 0 % 3 RED
    0 1 1 % 4 CYAN
    1 0.5 0.25 % 5 ORANGE
    1 1 0 % 6 YELLOW (pale)
    0.7 0.7 0.7 % 7 GREY
    0 0.75 0.75 % 8 TURQUOISE
    0 0.5 0 % 9 GREEN (dark)
    0.75 0.75 0 % 10 YELLOW (dark)
    1 0 1 % 11 MAGENTA (pale)
    0.75 0 0.75 % 12 MAGENTA (dark)
    0 0 0 % 13 BLACK
    0.8 0.7 0.6 % 14 BROWN (pale)
    0.6 0.5 0.4 ]; % 15 BROWN (dark)
axes(plotAxes);
cla
imshow(I, [], 'InitialMagnification','fit')
axis equal
hold on
c = mytree.findleaves;
for i=c
    if dt1.get(i) > 0    % if any peak exist in the leaf
        d(1:4) = mytree.get(i).ranges;
        plot([d(3), d(4), d(4), d(3), d(3)], [d(1), d(1), d(2), d(2),d(1)],...
            'LineWidth', 2.4, 'Color', colorOrder(dt.get(i)+1,:));
    end
end

end

function save_image(~,~, plotAxes)
plotName = strcat('fft_tree_decomposition-',datestr(datetime('now')), '.png');
fr = getframe(plotAxes);
imwrite(fr.cdata, plotName);
disp(['Saving to FFT decomposition image to file:', plotName]);
end

