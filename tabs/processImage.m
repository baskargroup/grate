function [output] = processImage(image, inputs)

%     level = graythresh(image);
    level = inputs.imgThreshold ;
    if inputs.usingAdaptiveThreshold
      bwImage = imAdaptThresh(imgaussfilt(image,1), inputs.adaptiveThresholdWindowSize, inputs.adaptiveThreshold,0);
    else
      bwImage = im2bw(imgaussfilt(image,1),level);
    end
    
    %imageThresholdSlider.Value =  level;
    %imageThresholdValueLabel.String =  num2str(level);

    % % main calculation happens here
    I_filt_bw_thin = bwmorph(bwImage, 'thin', Inf);
    I_thinned = I_filt_bw_thin - bwmorph(I_filt_bw_thin,'branchpoints');
    I_temp = I_thinned;
    cc = bwconncomp(I_temp);
    numPixels = cellfun(@numel,cc.PixelIdxList);
    %pixelThreshold = str2double(pixelThresholdEdit.String);

    for k = 1:length(numPixels)
      if(numPixels(k) < inputs.pixelThreshold)
        I_temp(cc.PixelIdxList{k}) = 0;
      end
    end
    I_thinned = I_temp;

    cc = bwconncomp(I_temp);
    s = regionprops(cc, 'Orientation', 'MajorAxisLength', 'MinorAxisLength', 'area', 'Centroid');

    % calculation related to finding the domains:
    % gather data:
    majAxis = [s.MajorAxisLength];
    minAxis = [s.MinorAxisLength];
    thetabar_all = [s.Orientation];
    centroid_temp = [s.Centroid];
    centroid_all = [];
    centroid_all(1,:) = centroid_temp(1:2:end);
    centroid_all(2,:) = centroid_temp(2:2:end);
    aspectRatio = minAxis ./ majAxis;
    %aspectRatioThreshold = str2double(aspectRatioThresholdEdit.String);
    aspectRatioThreshold = inputs.aspectRatioThreshold;
    aspRatioFlag = false(size(aspectRatio));
    aspRatioFlag(aspectRatio <= aspectRatioThreshold) = true;
    ifaspRatioThresh = inputs.usingAspectRatioThreshold;

    % get the connected ellipse graph edges:
    lenS = length(majAxis);     % = length(handles.s);
%     L = 0*eye(lenS);
    rowList = [];
    columnList = [];
    half_dr = utils.convertnm2Pix(inputs.pixelScale, inputs.deltaR);
    angleTol = inputs.polymerFlexibility;
    for k = 1:lenS
%       % check for the ellipses in the vicinity of the current ellipse
      ellipselist = find((centroid_all(2,:) - centroid_all(2,k) ).^2 + ...
        abs(centroid_all(1,:) - centroid_all(1,k) ).^2<=half_dr^2);
      if(ifaspRatioThresh)
        ellipselist = ellipselist(aspRatioFlag(ellipselist));
      end
      % iterate over the neighbouring ellipses
      for jj = 1:size(ellipselist,2)
        % if the angles are close by, make the corresponding entry true
        if (abs(thetabar_all(ellipselist(jj)) - thetabar_all(k) ) < angleTol)
%           L(k,ellipselist(jj)) = 1;
          rowList = [rowList;k];
          columnList = [columnList;ellipselist(jj)];
        end
      end
    end
%     
%     L_sparse = sparse(L);
%     try
      L_sparse2 = sparse([rowList;columnList], [columnList;rowList], 1);
      L_sparse = L_sparse2;
%     catch
%       display('Issues with constructing connectivity matrix!');
%     end

    % get connected components
    [S,C] = graphconncomp(L_sparse);

    % index the labels with respect to the frequency
    [counts, centers] = histcounts(C,1:S+1);
    % makes the bins to be the same as conn comp labels
    centers = centers(1:end-1);
    %TODO filter out groups with < 10 components
    %counts_filtered = counts > 9
    indices = find(counts > 5);
    centers = centers(:,indices);
    counts = counts(indices);
    
    histData = [round(centers'),counts'];
    clear centers counts
    % sort the histData wrt the frequency
    [histData_indexed,indx] = sortrows(histData,2);
    

    output = process_image_results(level,...
        bwImage,...
        I_thinned,...
        cc,...
        s,...
        majAxis,...
        minAxis,...
        thetabar_all,...
        centroid_all,...
        aspectRatio,...
        aspRatioFlag,...
        L_sparse,...
        S,...
        C,...
        histData,...
        histData_indexed,...
        indx);

    % Process completed. plotting processes will be done when the respective
    % plot function is called.

end
