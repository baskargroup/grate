classdef plots_tab < handle
    %Needs to subclass handle to be passed by ref, not by copy
    
    
    properties
        plotsTabRef
        
        %Defaults
        defaultNumDomains = 1;
        defaultNumGroups = 1;
        
        %Inputs
        imageProcessingResults
        imageProcessingInputs
        originalImage
        
        %Layout constants:
        pickerStartX = .3;
        pickerWidth = .45;
        
        %UI Controls:
        samplePlotAxes
        
        plotsPanel
        basicPlottingPanel
        histogramPlottingPanel
        
        infoToPlotLabel
        backgroundImageLabel
        plotGroupsLabel
        numberGroupsLabel
        totalGroupsLabel
        domainNumberLabel
        usingAspectRatioLabel
        
        numberGroupsEdit
        domainNumberEdit
        
        backBtn
        nextBtn
        plotGroupInfoBtn
        plotComponentInfoBtn
        testHistogramSettingsBtn
        savePlotImageBtn
        globalFftBtn
        localFftBtn
        
        withNamesCheckbox
        withArThresholdCheckbox
        
        backgroundImagePicker
        infoToPlotPicker
        plotGroupsPicker
        
    end
    
    methods
        
        function obj=plots_tab(plotsTabReference, inputs, imageProcessingResults, image)
            
            obj.plotsTabRef = plotsTabReference;
            obj.imageProcessingInputs = inputs;
            obj.imageProcessingResults = imageProcessingResults;
            obj.originalImage = image;
            
            obj.plotsPanel = uipanel('Parent', obj.plotsTabRef,...
                'Units', 'normalized',...
                'Position', [0.03, 0.13, 0.43, 0.84],...
                'Title', 'Plots to Generate');
            
            obj.backBtn = uicontrol('Parent', obj.plotsTabRef,...
                'Units', 'normalized',...
                'Position', [0.05, 0.05, 0.1, 0.05],...
                'Style', 'pushbutton',...
                'FontSize', 10,...
                'String', 'Back',...
                'Visible', 'on');
            
            obj.nextBtn = uicontrol('Parent', obj.plotsTabRef,...
                'Units', 'normalized',...
                'Position', [0.85, 0.05, 0.1, 0.05],...
                'Style', 'pushbutton',...
                'FontSize', 10,...
                'String', 'Next',...
                'Visible', 'on');
            
            obj.basicPlottingPanel = uipanel('Parent', obj.plotsPanel,...
                'Units', 'normalized',...
                'Position', [0.03, 0.5, 0.94, 0.47],...
                'Title', 'Basic Plotting');
            
            obj.backgroundImageLabel = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [0.03, 0.83, 0.24, 0.15],...
                'Style', 'text',...
                'HorizontalAlignment', 'right',...
                'String', 'Background image: ');
            
            obj.infoToPlotLabel = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [0.03, 0.65, 0.24, 0.15],...
                'Style', 'text',...
                'HorizontalAlignment', 'right',...
                'String', 'Info. to plot: ');
            
            obj.plotGroupsLabel = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [0.03, 0.47, 0.24, 0.15],...
                'Style', 'text',...
                'HorizontalAlignment', 'right',...
                'String', 'Groups info. to plot: ');
            
            obj.numberGroupsLabel = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [0.03, 0.29, 0.24, 0.15],...
                'Style', 'text',...
                'HorizontalAlignment', 'right',...
                'String', '# of Groups: ');
            
            obj.backgroundImagePicker = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX, 0.83, 0.54, 0.15],...
                'Style', 'popupmenu',...
                'String', {'Select Background Image for Plotting Domain',...
                'Original','Black & White','Skeleton', 'White'},...
                'Callback', @obj.backgroundImageCallback);
            
            obj.infoToPlotPicker = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX, 0.65, obj.pickerWidth, 0.15],...
                'Style', 'popupmenu',...
                'String', {'Select Component Information to Plot','Lines','Ellipsoids'},...
                'Callback', @obj.infoToPlotCallback);
            
            obj.plotGroupsPicker = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX, 0.47, obj.pickerWidth, 0.15],...
                'Style', 'popupmenu',...
                'String', {'Select Group Information to Plot', 'Only Groups',...
                'Groups with like oriented ellipsoids',...
                'Groups with like oriented lines',...
                'Groups with all ellipsoids', 'Groups with all lines',...
                'Like oriented lines only'},...
                'Callback', @obj.plotGroupsPickerCallback);
            
            obj.numberGroupsEdit = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX, 0.37, obj.pickerWidth / 2 - .05, 0.09],...
                'Style', 'edit',...
                'String', num2str(obj.defaultNumGroups),...
                'Callback', @obj.numberGroupsCallback);
            
            usingAspectRatioLabelString = 'Not using aspect ratio threshold';
            if obj.imageProcessingInputs.usingAspectRatioThreshold == 1
                usingAspectRatioLabelString = 'Using aspect ratio threshold';
            end
            obj.usingAspectRatioLabel = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX, 0.25, 0.5, 0.08],...
                'Style', 'text',...
                'HorizontalAlignment', 'left',...
                'String', usingAspectRatioLabelString);
            
            obj.withNamesCheckbox = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX, 0.16, 0.34, 0.08],...
                'Style', 'checkbox',...
                'HorizontalAlignment', 'left',...
                'String', 'Plot with names');
            
            obj.plotComponentInfoBtn = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX + obj.pickerWidth + .03, .71, .15, .09],...
                'Style', 'pushbutton',...
                'String', 'Plot',...
                'Callback', @obj.plotComponentInfoOnClick);
            
            obj.plotGroupInfoBtn = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX + obj.pickerWidth + .03, .54, .15, .09],...
                'Style', 'pushbutton',...
                'String', 'Plot',...
                'Callback', @obj.plotGroupInfoOnClick);
            
            obj.totalGroupsLabel = uicontrol('Parent', obj.basicPlottingPanel,...
                'Units', 'normalized',...
                'Position', [obj.pickerStartX + obj.pickerWidth / 2, .37, obj.pickerWidth, .09],...
                'Style', 'text',...
                'HorizontalAlignment', 'left',...
                'String', strcat('(', num2str(size(obj.imageProcessingResults.histData_indexed,1)), ' total groups)'));
            
            obj.histogramPlottingPanel = uipanel('Parent', obj.plotsPanel,...
                'Units', 'normalized',...
                'Position', [.03, .1, .94, .37],...%[.03, .2, .94, .27],...
                'Title', 'Other Plots');%'Histogram Plots');
            
            obj.domainNumberLabel = uicontrol('Parent', obj.histogramPlottingPanel,...
                'Units', 'normalized',...
                'Position', [.05, .71, .35, .16],...
                'Style', 'text',...
                'HorizontalAlignment', 'left',...
                'Visible', 'off',...
                'String', 'Histogram for Domain Number: ');
            
            obj.domainNumberEdit = uicontrol('Parent', obj.histogramPlottingPanel,...
                'Units', 'normalized',...
                'Position', [.4, .74, .1, .15],...
                'Style', 'edit',...
                'Visible', 'off',...
                'String', num2str(obj.defaultNumDomains));
            
            obj.testHistogramSettingsBtn = uicontrol('Parent', obj.histogramPlottingPanel,...
                'Units', 'normalized',...
                'Position', [.55, .74, .4, .15],...
                'Style', 'pushbutton',...
                'String', 'Test Histogram Plot',...
                'Visible', 'off',...    %Histogram is not currently working, don't give option
                'Callback', @obj.testHistogramSettingsCallback);
            
            obj.globalFftBtn = uicontrol('Parent', obj.histogramPlottingPanel,...
                'Units', 'normalized',...
                'Position', [.05, .4, .4, .15],...%[.25, .2, .5, .4],...
                'Style', 'pushbutton',...
                'String', 'Global FFT Decomposition');
            
            obj.localFftBtn = uicontrol('Parent', obj.histogramPlottingPanel,...
                'Units', 'normalized',...
                'Position', [.05, .2, .4, .15],...%[.25, .2, .5, .4],...
                'Style', 'pushbutton',...
                'String', 'Local FFT Decomposition',...
                'Visible','off');
            
            obj.samplePlotAxes = axes('Parent', obj.plotsTabRef,...
                'Units', 'normalized',...
                'Position', [.53, .35, .42, .6],...%[.53, .15, .42, .6],...
                'XTickMode', 'manual',...
                'XTick', [],...
                'XTickLabel', [],...
                'XColor', 'none',...
                'YTick', [],...
                'YTickLabel', [],...
                'YColor', 'none');
            
            obj.savePlotImageBtn = uicontrol('Parent', obj.plotsTabRef,...
                'Units', 'normalized',...
                'Position', [.7, .25, .1, .05],...
                'Style', 'pushbutton',...
                'String', 'Save Plot Image',...
                'Callback', @obj.savePlotImageCallback);
        end
        
        function backgroundImageCallback(obj, ~, ~)
            
        end
        
        function withArThresholdCallback(obj, ~, ~)
            % obj.usingAspectRatioThreshold = withArThresholdCheckbox.Value;
        end
        
        function savePlotImageCallback(obj, ~, ~)
            plotName = strcat('plotImage-',datestr(datetime('now')));
            plotName = strcat(plotName, '-', ...
                char(obj.backgroundImagePicker.String(obj.backgroundImagePicker.Value)), ...
                '-', num2str(obj.infoToPlotPicker.Value), ...
                '-', char(obj.plotGroupsPicker.String(obj.plotGroupsPicker.Value)));
            axes(obj.samplePlotAxes);
            hfig = figure('visible','off');
            hax_new = copyobj(obj.samplePlotAxes, hfig);
            set(hax_new, 'Position', get(0, 'DefaultAxesPosition'));
            colormap(hax_new,'gray')
            print(gcf, '-dpng', plotName)
            disp(['Saving to file:', plotName, '.png']);
        end
        
        function infoToPlotCallback(obj, ~, ~)
            %obj.plotInfo = obj.infoToPlotPicker.Value - 1;
        end
        
        function plotGroupsPickerCallback(obj, ~, ~)
            %             domainPlot = plotGroupsPicker.Value - 1;
            %             numGroups = str2double(numberGroupsEdit.String);
            %
            %             ifNameDomains = withNamesCheckbox.Value;
        end
        
        function numberGroupsCallback(obj, ~, ~)
            numGroups = str2double(obj.numberGroupsEdit.String);
            %disp(sprintf('Plotting %d largest domains',numGroups));
            %obj.processImageResults = processImageWrapper();
            
            if numGroups > obj.imageProcessingResults.S
                error('You asked for more domains than identified. Please modify delta x, delta y  and delta theta for more domains');
            end
        end
        
        function plotOptions=getPlotOptions(obj)
            plotComponents = -1;
            plotGroups = -1;   % only matter in production runs
            
            backgroundOption = obj.backgroundImagePicker.Value;
            %2 = original, 3 = b&w, 4 = skeleton, 5 = white
            
            switch backgroundOption
                case 1
                    error('Please select a background image');
                case 2                    % original image
                    I_bg = obj.originalImage;
                    display('Plotting on original image');
                case 3                    % thresholded image
                    I_bg = obj.imageProcessingResults.bwImage;
                    display('Plotting on thresholded image');
                case 4                    % skeletonized image
                    I_bg = obj.imageProcessingResults.I_thinned;
                    display('Plotting on skeletonized image');
                case 5
                    I_bg = ones(size(obj.originalImage));
                    display('Plotting on WHITE background! ')
                otherwise
                    error('Check the value sent to background image');
            end
            
            componentInfoToPlot = obj.infoToPlotPicker.Value; % 2 = lines, 3 = ellipsoids
            
            groupInfoToPlot = obj.plotGroupsPicker.Value;
            numGroupsToPlot = str2double(obj.numberGroupsEdit.String);
            
            plotWithNames = obj.withNamesCheckbox.Value;
            
            plotHistograms = -1; %Only matters in production runs
            
            plotOptions = plot_options(I_bg, plotComponents, plotGroups,...
                backgroundOption, componentInfoToPlot, groupInfoToPlot,...
                numGroupsToPlot, plotWithNames,...
                plotHistograms, false, false);
        end
        
        function plotComponentInfoOnClick(obj, ~, ~)
            plotOptions = getPlotOptions(obj);
            handleToMessageBox = msgbox('Please wait...');
            
            plot_component_info(obj.imageProcessingInputs,...
                obj.imageProcessingResults, plotOptions, obj.samplePlotAxes);
            
            if exist('handleToMessageBox', 'var')
                delete(handleToMessageBox);
                clear('handleToMessageBox');
            end
        end
        
        function plotGroupInfoOnClick(obj, ~, ~)
            plotOptions = getPlotOptions(obj);
            handleToMessageBox = msgbox('Please wait...');
            % Do stuff that takes a long time.....
            plot_group_info(obj.imageProcessingInputs,...
                obj.imageProcessingResults, plotOptions,...
                obj.samplePlotAxes);
            if exist('handleToMessageBox', 'var')
                delete(handleToMessageBox);
                clear('handleToMessageBox');
            end
        end
        
        function testHistogramSettingsCallback(obj, ~, ~)
            
            groupToPlot = str2double(obj.domainNumberEdit.String);
            plotOptions = getPlotOptions(obj);
            plot_histogram_for_group(groupToPlot, obj.imageProcessingInputs,...
                obj.imageProcessingResults, plotOptions,...
                obj.samplePlotAxes);
            
        end
        
    end
    
end