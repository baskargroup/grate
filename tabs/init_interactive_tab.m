function init_interactive_tab(varargin)
global interactivePlotAxis
pixelScale = 0;
imageFileName = '';
if nargin < 1
    [imageFileName, foldername]  = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.img; *.IMG; *.tif; *.TIF; *.tiff, *.TIFF'},...
        'Select image to interact with');
    addpath(genpath(foldername));
    %   addpath(genpath('../'));
    image = imread(imageFileName);
    interactiveTabRef = figure('Visible', 'on', 'Position', [100, 100, 1100, 700], 'Toolbar', 'none');
    pixelScale = 4.2;
    rmpath(genpath(foldername));
else
    interactiveTabRef = varargin{1};
    image = varargin{2};
    assert(all( size(size(image)) == [1,2]))
    pixelScale = varargin{3};
    imageFileName = varargin{4};
end

file_name = char(imageFileName);
fileID = fopen(strcat('FFTI_',file_name(1:end-4)),'a');
fprintf(fileID, 'Location(x)  Location(y)   Lx(nm)  Ly(nm)  Area(nm^2)   d1  d2  |theta1-theta2|   \n');
fclose(fileID);
iter = 0;

h = 0;

instructionsLabel = uicontrol('Parent', interactiveTabRef,...
    'Units', 'normalized',...
    'Position', [.05, .87, .7, .1],...
    'Style', 'text',...
    'FontSize', 11,...
    'String', ['Begin by clicking and dragging your mouse over the image on the left to create a rectangle around the area of interest. ',...
    'Double click the rectangle you create to start the FFT decomposition. It will plot the results on the right image. ',...
    'When it is finished, you can click More FFTs',...
    ' and then drag the previously used rectangle to another region, double-click, and compute another FFT decomposition.']);

interactiveAxis = axes('Parent', interactiveTabRef,...
    'Units', 'normalized',...
    'Position', [.03, .25, .35, .6],...
    'XTickMode', 'manual',...
    'XTick', [],...
    'XTickLabel', [],...
    'XColor', 'none',...
    'YTick', [],...
    'YTickLabel', [],...
    'YColor', 'none');

interactivePlotAxis = axes('Parent', interactiveTabRef,...
    'Units', 'normalized',...
    'Position', [.4, .25, .35, .6],...
    'Visible','off',...
    'XTickMode', 'manual',...
    'XTick', [],...
    'XTickLabel', [],...
    'XColor', 'none',...
    'YTick', [],...
    'YTickLabel', [],...
    'YColor', 'none');

saveRelAngleInfoButton = uicontrol('Parent', interactiveTabRef,...
    'Units', 'Normalized',...
    'Position', [0.85, 0.80, 0.10,0.05],...
    'Style', 'pushbutton',...
    'String', 'Save Relative Angle Info',...
    'Visible', 'off',...
    'Callback', @saveRelAngleInfo);

clearInfoButton = uicontrol('Parent', interactiveTabRef,...
    'Units', 'Normalized',...
    'Position', [0.85, 0.90, 0.10, 0.05],...
    'Style', 'pushbutton',...
    'String', 'Clear data',...
    'Visible', 'off',...
    'Callback', @clearInfo);

resultsPanel = uipanel('Parent', interactiveTabRef,'Title', 'FFT Results',...
    'Position',[0.76 0.45 0.22 0.35]);
pairsListStartX = .77;
pairsListWidth = .2;
pairsList = uicontrol('Parent', resultsPanel,...
    'Units', 'normalized',...
    'Position', [0.05, 0.05, 0.8, 0.75],...
    'Style', 'listbox',...
    'FontSize', 12);
% 'Position', [pairsListStartX, 0.47, pairsListWidth, 0.25],...
%'String', 'Pairs of Peaks');

pairsDistanceHeaderLabel = uicontrol('Parent', resultsPanel,...
    'Units', 'normalized',...
    'Position', [0.12, 0.82, 0.4, 0.1],...
    'Style', 'text',...
    'FontSize', 10,...
    'HorizontalAlignment', 'Left',...
    'String', 'Distance (nm)');

pairsOrientationHeaderLabel = uicontrol('Parent', resultsPanel,...
    'Units', 'normalized',...
    'Position', [0.552, 0.82, 0.4, 0.1],...
    'Style', 'text',...
    'FontSize', 10,...
    'HorizontalAlignment', 'Left',...
    'String', 'Orientation (degrees)');

interactiveStatusLabel = uicontrol('Parent', interactiveTabRef,...
    'Units', 'normalized',...
    'Position', [.4, .15, .35, .08],...
    'Style', 'text',...
    'FontSize', 12,...
    'String', '');

moreFFTsBtn = uicontrol('Parent', interactiveTabRef,...
    'Units', 'Normalized',...
    'Position', [.5, .1, .1, .05],...
    'Style', 'pushbutton',...
    'String', 'More FFTs',...
    'Visible', 'off',...
    'Callback', {@runInteractiveTab, interactivePlotAxis });

postProcessPanel = uipanel('Parent', interactiveTabRef,'Title', 'Postprocess',...
    'Position',[0.66 0.05 0.3 0.15]);
pullDown1=uicontrol('Parent',postProcessPanel,...
    'Units','Normalized',...
    'Position',[0.05, 0.6, 0.4, 0.3],...
    'Style','popupmenu',...
    'Visible','off');
pullDown2=uicontrol('Parent',postProcessPanel,...
    'Units','Normalized',...
    'Position',[0.5, 0.6, 0.4, 0.3],...
    'Style','popupmenu',...
    'Visible','off');
calcAngleBtn = uicontrol('Parent', postProcessPanel,...
    'Units', 'Normalized',...
    'Position', [0.05, 0.15, 0.4, 0.3],...
    'Style', 'pushbutton',...
    'String', 'Calculate Relative Angle',...
    'Visible', 'off',...
    'CallBack',@showRelAngle);
relAngleStr = uicontrol('Parent', postProcessPanel,...
    'Units', 'Normalized',...
    'Position', [0.5, 0.15, 0.4, 0.3],...
    'Style', 'text',...
    'String', '',...
    'FontSize',16,...
    'Visible', 'off');
fft_pairs = [];
fft_peaks = [];
totalSize = [];
axes(interactivePlotAxis); hold on; cla; hold off

runInteractiveTab(0,0,interactivePlotAxis);

    function runInteractiveTab(~,~, interactivePlotAxis)
       
        relAngleStr.Visible = 'off';
        pullDown1.Visible = 'off';
        pullDown2.Visible = 'off';
        calcAngleBtn.Visible =  'off';
        
        thres2 = 5; % thresold for the thresholded and cleaned fft to identify peaks
        
        I_orig = image; %imread(image);
        I = I_orig;
        
        % marker selection of image
        if(iter==0)
            %fig_original = figure;
            axes(interactiveAxis);
            imshow(I_orig,'InitialMagnification','fit'); axis square
            h = imrect;
            ax_original = gca;
            position=wait(h);
        else
            %figure(fig_original);
            axes(interactiveAxis);
            h = imrect(interactiveAxis,getPosition(h));
            %addNewPositionCallback(h,@(p) title(sprintf('Selection area:%s',mat2str(p,3))));
            fcn = makeConstrainToRectFcn('imrect',get(interactiveAxis,'XLim'),get(interactiveAxis,'YLim'));
            setPositionConstraintFcn(h,fcn);
            position=wait(h);
        end
         
        handleToMessageBox = msgbox('Please wait...');
        axes(interactivePlotAxis); hold on; cla; hold off
        
        interactiveStatusLabel.Visible = 'on';
        interactiveStatusLabel.String = 'Computing...';
        
        I = imcrop(I_orig,getPosition(h));                      % crop original image
        
        % fft routines
        % I_gf = imgaussfilt(I);
        I_gf = I;
        I = imresize(I_gf,1);
        
        % take fft and do necessary shifts
        F = fft2(I);
        F = fftshift(F);        % Center FFT
        F = abs(F);             % Get the magnitude
        F = log(F+1);           % Use log, for perceptual scaling, and +1 since log(0) is undefined
        
        F_filtered = F;
        
        
        x = (1:size(F_filtered,2))-round(size(F_filtered,2)/2);
        y = (1:size(F_filtered,1))-round(size(F_filtered,1)/2);
        [X,Y] = meshgrid(x,y);
        [fitresult, gof] = createFit(X,Y,F_filtered);        % calculates using fitting, the best fit surface
        % fit equation: z = a + c*exp(-((x.^2) /d1 + (y.^2)/d2 ))
        %     a = 9.084; c = 2.217; d1 = 1.5925e5; d2 = 8.557e4;
        a = fitresult.a;
        c = fitresult.c;
        d1 = fitresult.d1;
        d2 = fitresult.d2;
        sub_surf= a + c * exp( -( (X.^2)./(d1) + (Y.^2)./(d2)));    % surface to be subtracted from the filtered FFT
        F_sub_fit = F_filtered - sub_surf;                          % fft that has the fitted surface subtracted from it
        F_positive = double(F_sub_fit > 2*std(F_sub_fit(:))) ;      % part of cleaned fft that is more than 2 std deviations
        f_cle = bwmorph(F_positive,'clean');                        % cleans single pixels
        
        % TODO : check number of thicken iterations -- keep it lower to not
        % move the centroids
        f_processed = bwmorph(f_cle,'thicken',5);                % thickens the pixels so that nearby components are joined
        f_cle = f_processed;
        
        
        % finding the connected components of the fft.
        CC=bwconncomp(f_cle);
        numPixels = cellfun(@numel,CC.PixelIdxList);
        % TODO : generalise this value : threshold for finding peaks
        thres2 = 3;
        
        for k = 1:length(numPixels)
            if(numPixels(k) < thres2)                                 % thres2 is based on the thicken strategy used.
                f_cle(CC.PixelIdxList{k}) = 0;
            end
        end
        
        CC=bwconncomp(f_cle);
        stats = regionprops(CC,'Centroid','MajorAxisLength','MinorAxisLength','Orientation','Area');
        
        num_peaks = CC.NumObjects;
        fft_peaks = zeros(num_peaks, 2);
        for peak = 1:num_peaks
            fft_peaks(peak,1) = stats(peak).Centroid(1);
            fft_peaks(peak,2) = stats(peak).Centroid(2);
        end
        
        [fft_peaks, fft_pairs, center] = fft_make_pairs(F, fft_peaks);
        
        totalX = center(1) * 2;
        totalY = center(2) * 2;
        totalSize = [totalX, totalY];
        
        startOffsetY = .8;
        labelHeight = .04;
        
        %Remove duplicate pairs:
        num_pairs = size(fft_pairs,1);
        to_delete = false(num_pairs, 1);
        for pair_index=1:num_pairs
            pair = fft_pairs(pair_index,:);
            if pair(2) > pair(1)
                to_delete(pair_index) = true;
            end
        end
        
        fft_pairs(to_delete,:) = [];
        num_pairs = size(fft_pairs,1);
        
        % destroy and make new axis for plotting:
        axes(interactivePlotAxis);
        cla;
        hold on;
        datacursormode on;
        
        title('Identified peaks of the FFT');
        imshow(F,[],'InitialMagnification','fit');
        pairsList.String = '';
        labelStr = [];
        for pair_index=1:num_pairs
            pair = fft_pairs(pair_index,:);
            
            pt1 = [fft_peaks(pair(1),1),fft_peaks(pair(1),2)];
            pt2 = [fft_peaks(pair(2),1),fft_peaks(pair(2),2)];
            
            deltaX = pt1(1) - pt2(1);
            deltaY = pt1(2) - pt2(2);
            
            dx = deltaX/totalX;
            
            dy = deltaY/totalY;
            
            distance = 2 / sqrt(dx*dx + dy*dy);
            distance = utils.convertPix2nm(pixelScale, distance);
            
            orientation = 90 - atan((pt1(2)-pt2(2))/(pt1(1)-pt2(1))) * (180 / pi);    % in degrees, in real space(90- compensates for the real space)
            distanceStr = sprintf('%0.2f',distance);
            orientationStr = sprintf('%0.2f',orientation);
            fft_peaks(pair(1), 3) = distance;
            fft_peaks(pair(2), 3) = distance;
            fft_peaks(pair(1),4) = orientation;
            fft_peaks(pair(2),4) = orientation;
            
            labelStr{pair_index} = [num2str(pair_index), '    ', distanceStr, '    ', orientationStr];
            menuStr{pair_index} = num2str(pair_index);
            
            %Plot pair of peaks:
            plot(pt1(1),pt1(2),'o','MarkerSize',6,'MarkerFaceColor','red', 'MarkerEdgeColor','Green');
            plot(pt2(1),pt2(2),'o','MarkerSize',6,'MarkerFaceColor','red', 'MarkerEdgeColor','Green');
            plot([pt1(1), pt2(1)],[pt1(2), pt2(2)],'--','Color', [153,142,195]/256);
            
            
            interactiveStatusLabel.String = sprintf('Plotting peak centroid pair %d / %d', pair_index, num_pairs);
            drawnow;
            pairsList.String = labelStr;
            pullDown1.String = menuStr;
            pullDown2.String = menuStr;
            saveRelAngleInfoButton.Visible = 'On';
            clearInfoButton.Visible = 'On';
            pause(02);
            
            
        end
        
        hold off;
        
        % close the wait message box, if it exists, once the calculations are complete
        if exist('handleToMessageBox', 'var')
            delete(handleToMessageBox);
            clear('handleToMessageBox');
        end
        
        
        iter = iter+1;
        
        % interactive input from user
        moreFFTsBtn.Visible = 'on';
        if(size(pullDown1.String) > 0)
            pullDown1.Visible = 'on';
            pullDown2.Visible = 'on';
            pullDown1.Value= 1;
            pullDown2.Value= 1;
            calcAngleBtn.Visible =  'on';
        end
        
        interactiveStatusLabel.String = sprintf('Done plotting (%d total pairs)', num_pairs);
        pairsList.String = labelStr;
        clear F
        interactivePlotAxis.Visible = 'Off';
    end

    function showRelAngle(~,~)
        
        pair1 = fft_pairs(pullDown1.Value,:);
        pair2 = fft_pairs(pullDown2.Value,:);
        
        vec1 = fft_peaks(pair1(1),:) - fft_peaks(pair1(2),:);
        vec2 = fft_peaks(pair2(1),:) - fft_peaks(pair2(2),:);
        angle1 = atand(vec1(2)/vec1(1));
        angle2 = atand(vec2(2)/vec2(1));
        
        relAngleStr.String = [num2str(abs(angle1 - angle2)),'(deg)'];
        relAngleStr.Visible = 'on';
    end

    function saveRelAngleInfo (~,~)
        
        try
            pair1 = fft_pairs(pullDown1.Value,:);
            pair2 = fft_pairs(pullDown1.Value,:);
            d1 = fft_peaks(pair1(1), 3);
            d2 = fft_peaks(pair2(1), 3);
            angle1 = fft_peaks(pair1(1), 4);
            angle2 = fft_peaks(pair2(1), 4);
            location =getPosition(h);
            %       disp('d1  d2  theta1  theta2  relAngle(deg)' )
            %       fprintf('%f, %f, %f, %f, %f \n ', d1, d2, angle1, angle2, abs(angle1 - angle2));
            fileID = fopen(strcat('FFTI_',file_name(1:end-4)),'a+');
            fprintf(fileID, '%f,  %f,   %f,   %f,   %f,   %f ,  %f,   %f    \n', location(1), ...
                location(2), ...
                utils.convertPix2nm(pixelScale, location(3)), ...
                utils.convertPix2nm(pixelScale, location(4)), ...
                utils.convertPix2nm(pixelScale, location(3))*utils.convertPix2nm(pixelScale, location(4)), ...
                d1, d2, abs(angle1-angle2));
            fclose(fileID);
        catch
            disp(getPosition(h));
        end
        
    end

    function clearInfo(~,~)
        disp('Clearing memory in interactive FFT tab. You need to hit "Back" and then come back to this tab!!')
        clear pixelScale
        clear location h fileID file_name
        clear pullDown1 pullDown2 clearInfoButton saveRelAngleInfoButton
        clear imageFileName foldername
    end

end
