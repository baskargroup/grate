%% Copyright 2014-2016 Baskar Ganapathysubramanian
%% 
%% This file is part of GRATE.
%% 
%% GRATE is free software: you can redistribute it and/or modify it under the
%% terms of the GNU Lesser General Public License as published by the Free
%% Software Foundation, either version 2.1 of the License, or (at your option)
%% any later version.
%% 
%% GRATE is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
%% A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
%% details.
%% 
%% You should have received a copy of the GNU Lesser General Public License along
%% with GRATE.  If not, see <http://www.gnu.org/licenses/>.

% --- end license text --- %
function bw = imAdaptThresh(IM,ws,C,tm)
% IMADAPTTHRESHOLD:  An adaptive thresholding algorithm that considers
% nonuniform illumination in the image.
% 
%  bw=imAdaptThresh(IM,ws,C) outputs a binary image bw with the local 
%   threshold mean-C or median-C to the image IM.
%  ws is the local window size.
%  tm is 0 or 1, a switch between mean and median. tm=0 mean(default); tm=1 median.
% 
%  For more information, please see
%  http://homepages.inf.ed.ac.uk/rbf/HIPR2/adpthrsh.htm

if (nargin<3)
    error('You must provide the image IM, the window size ws, and C.');
elseif (nargin==3)
    tm=0;
elseif (tm~=0 && tm~=1)
    error('tm must be 0 or 1.');
end

IM=mat2gray(IM);
ws = floor(ws);

if tm==0
    mIM=imfilter(IM,fspecial('average',ws),'replicate');
else
    mIM=medfilt2(IM,[ws ws]);
end
sIM=mIM-IM-C;
bw=im2bw(sIM,0);
bw=imcomplement(bw);
