function init_production_tab(productionTab)
    productionTabControlHeight = .05;
    productionImagesSelectBtn = uicontrol('Parent', productionTab,...
        'Units', 'normalized',...
        'Position', [.03, .65, .15, productionTabControlHeight],...
        'Style', 'pushbutton',...
        'String', 'Select Images Folder',...
        'Callback', @productionImagesSelectOnClick);

    productionFolderName = '';

    function productionImagesSelectOnClick(~, ~)
        % hObject    handle to button_browse (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    structure with handles and user data (see GUIDATA)
        productionFolderName = uigetdir('.', 'Select directory of images to process');
        if productionFolderName~=0

          productionFilePath.String = productionFolderName;
          %set(handles.text_filepath,'string',[handles.folder_name])

        end

    end

    productionFilePath = uicontrol('Parent', productionTab,...
        'Units', 'normalized',...
        'BackgroundColor', 'w',...
        'Position', [.2, .65, .28, productionTabControlHeight],...
        'String', 'File path',...
        'Style', 'text');

    productionSettingsSelectBtn = uicontrol('Parent', productionTab,...
        'Units', 'normalized',...
        'Position', [.03, .55, .15, productionTabControlHeight],...
        'Style', 'pushbutton',...
        'String', 'Select Settings File',...
        'Callback', @productionSettingsSelectOnClick);

    function productionSettingsSelectOnClick(~, ~)
        % hObject    handle to button_browse (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    structure with handles and user data (see GUIDATA)
        folderName = uigetfile('./*.mat', 'Select a previously generated settings file');
        if folderName~=0
          productionSettingsFilePath.String = folderName;
          productionProcessBtn.Visible = 'on';
        else 
            productionProcessBtn.Visible = 'off';
        end

    end

    productionSettingsFilePath = uicontrol('Parent', productionTab,...
        'Units', 'normalized',...
        'BackgroundColor', 'w',...
        'Position', [.2, .55, .28, productionTabControlHeight],...
        'String', 'File path',...
        'Style', 'text');

    productionProcessBtn = uicontrol('Parent', productionTab,...
        'Units', 'normalized',...
        'Position', [.2, .45, .1, .05],...
        'Style', 'pushbutton',...
        'FontSize', 10,...
        'String', 'Process',...
        'Visible', 'Off',...
        'Callback', @productionProcessOnClick);

    productionStatusLabel = uicontrol('Parent',  productionTab,...
        'Units', 'normalized',...
        'Position', [.2, .35, .1, .05],...
        'Style', 'text',...
        'Visible', 'Off');

    function productionProcessOnClick(~, ~)
        settings = load(productionSettingsFilePath.String);
        inputs = settings.inputs;
        plotOptions = settings.plotOptions;
        
        if productionFolderName~=0
            
            fig = figure('Visible', 'off');
            plottingAxes = axes('Visible', 'off', 'Parent', fig,...
                'Units', 'normalized',...
                'Position', [0, 0, 1, 1]);

            fileStruct = [dir([productionFolderName '/*.jpg']);dir([productionFolderName '/*.tiff' ]); dir([productionFolderName '/*.tif' ]); dir([productionFolderName '/*.bmp' ]); dir([productionFolderName '/*.png' ])];
            n = length(fileStruct);


          %Begin processing:
          productionStatusLabel.Visible = 'on';
          for i = 1:n
            productionStatusLabel.String = strcat('Processing image ', num2str(i), ' out of ', num2str(n));
            name =  fileStruct(i).name;
            imageFileName = fullfile(productionFolderName, name);
            %disp(imageFileName);
            imageToProcess = imread(char(imageFileName));

            imageProcessResults = processImage(imageToProcess, inputs);
            switch plotOptions.backgroundImage
              case 0
                %error('Please select a background image');
              case 1                    % original image
                plotOptions.I_bg = imageToProcess;
                %display('Plotting on original image');
              case 2                    % thresholded image
                plotOptions.I_bg = imageProcessResults.bwImage;
                %display('Plotting on thresholded image');
              case 3                    % skeletonized image
                plotOptions.I_bg = imageProcessResults.I_thinned;
                %display('Plotting on skeletonized image');
              otherwise
                %error('Check the value sent to background image');
            end
              %check each plot setting -> create & save each
              %If runFFT -> Run FFT and save data to csv file
              
            if plotOptions.plotComponents == 1
                plot_component_info(inputs, imageProcessResults, plotOptions, plottingAxes);
                saveas(fig, strcat(imageFileName, '_components.png'));
            end
            
            if plotOptions.plotGroups == 1
                plot_group_info(inputs, imageProcessResults, plotOptions, plottingAxes);
                saveas(fig, strcat(imageFileName, '_groups.png'));
            end
            
            if plotOptions.plotHistograms == 1
                for group=1:imageProcessResults.S
                    plot_histogram_for_group(group, inputs, imageProcessResults, plotOptions, plottingAxes);
                    saveas(fig, strcat(imageFileName, '_histogram_group_', num2str(group), '.png'));
                end
            end
            
            if plotOptions.plotGlobalFFT == 1 
                dummyAxes = axes('Visible', 'off', 'Parent', fig,...
                    'units', 'normalized', 'position', [0, 0, 0, 0]);
                
                fft_data = plot_FFT(imageToProcess, imageProcessResults, dummyAxes, plottingAxes, true);
                saveas(fig, strcat(imageFileName, '_fft_global_decomp', '.png'));
                
                if isempty(fft_data)
                    disp('empty fft_data');
                else
                    writeFftToCsv(strcat(imageFileName, '_fft_global_data.csv'), fft_data);
                end
            end
            
            if plotOptions.plotLocalFFT == 1 
                dummyAxes = axes('Visible', 'off', 'Parent', fig,...
                    'units', 'normalized', 'position', [0, 0, 0, 0]);
                
                fft_data = plot_FFT(imageToProcess, imageProcessResults, dummyAxes, plottingAxes, false);
                saveas(fig, strcat(imageFileName, '_fft_local_decomp', '.png'));
                
                if isempty(fft_data)
                    disp('empty fft_data');
                else
                    writeFftToCsv(strcat(imageFileName, '_fft_local_data.csv'), fft_data);
                end
            end
            

          end

          if isempty(fileStruct)
              productionStatusLabel.String = 'No images found in the supplied folder';
              return
          end

          productionStatusLabel.String = 'All images processed';

        end
    end

    function writeFftToCsv(output_file_name, fft_data)
        num_data = length(fft_data);
        data_rows = zeros(num_data, 3);
        
        for fft_index = 1:num_data
            row = [fft_data(fft_index).Centroid, fft_data(fft_index).latt_spac, fft_data(fft_index).latt_orient];
            data_rows(fft_index,:) = row;
        end
        
        csvwrite(output_file_name, data_rows);
    end
end