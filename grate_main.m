% Copyright 2014-2016 Baskar Ganapathysubramanian
%
% This file is part of GRATE.
%
% GRATE is free software: you can redistribute it and/or modify it under the
% terms of the GNU Lesser General Public License as published by the Free
% Software Foundation, either version 2.1 of the License, or (at your option)
% any later version.
%
% GRATE is distributed in the hope that it will be useful, but WITHOUT ANY
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
% FOR
% A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
% details.
%
% You should have received a copy of the GNU Lesser General Public License along
% with GRATE.  If not, see <http://www.gnu.org/licenses/>.

% --- end license text --- %

function grate_main

addpath(genpath('./tabs'));
addpath(genpath('./plots'));
addpath(genpath('./models'));
addpath(genpath('./fft'));
addpath(genpath('./utils'));

global interactivePlotAxis        % in order to control the overlapping plots in interactive fft tab
global fftOriginalImageAxes

%  Create and then hide the UI as it is being constructed.
f = figure('Visible', 'off', 'Position', [300, 300, 1100, 700], 'Toolbar', 'none');
tabgroup = uitabgroup('Parent', f);
startTab = uitab('Parent', tabgroup, 'Title', 'Start');
imageTab = uitab('Parent', tabgroup, 'Title', 'Image');
interactiveTab = uitab('Parent', tabgroup, 'Title', 'Interactive');
plotsTab = uitab('Parent', tabgroup, 'Title', 'Plots');
fftTab   = uitab('Parent', tabgroup, 'Title', 'FFT');
saveTab  = uitab('Parent', tabgroup, 'Title', 'Save');
productionTab = uitab('Parent', tabgroup, 'Title', 'Production');

%Flag to indicate whether user pressed 'production'
% from the start page or went through configuration
straightToProduction = 0;

%Initial tab
grateInfoPanel = uipanel('Parent', startTab,...
    'BackgroundColor', [.93, .84, .84],...
    'ShadowColor', [1, 0, 0, 1],...
    'Units', 'normalized',...
    'Position', [0.2, 0.5, 0.6, 0.4]);

grateTitle = uicontrol('Parent', grateInfoPanel,...
    'FontName', 'MS Sans Serif',...
    'Style', 'text',...
    'FontSize', 24,...
    'Units', 'normalized',...
    'Position', [0.12, 0.7, 0.75, 0.3],...
    'String', 'GRATE: GRaph Analysis of TEM images');

infoString = sprintf('Software developed by Ganapathysubramanian group (ISU) and Chabinyc group (UCSB) \n\nLead Developer: Balaji S. Sarath Pokuri\nTesting: Kathryn O-Hara\nGUI : Jacob Stimes');

grateInfo = uicontrol('Parent', grateInfoPanel,...
    'Units', 'normalized',...
    'Position', [0.02, 0.075, 0.8, 0.45],...
    'BackgroundColor', grateInfoPanel.BackgroundColor,...
    'Style', 'text',...
    'String', infoString,...
    'FontSize', 12,...
    'HorizontalAlignment', 'center');

configureBtn = uicontrol('Parent', startTab,...
    'Units', 'normalized',...
    'Position', [0.25, 0.3, 0.2, 0.1],...
    'BackgroundColor', grateInfoPanel.BackgroundColor,...
    'Style', 'pushbutton',...
    'String', 'Configure Settings',...
    'FontSize', 16,...
    'Callback', @configureOnClick);

productionBtn = uicontrol('Parent', startTab,...
    'Units', 'normalized',...
    'Position', [0.55, 0.3, 0.2, 0.1],...
    'Style', 'pushbutton',...
    'String', 'Production',...
    'FontSize', 16,...
    'Callback', @productionOnClick);

    function configureOnClick(source, event)
        straightToProduction = 0;
        tabgroup.SelectedTab = imageTab;
    end

    function productionOnClick(source, event)
        straightToProduction = 1;
        tabgroup.SelectedTab = productionTab;
    end

%Inputs tab
inputsTabObj = inputs_tab(imageTab);
inputsTabObj.interactiveBtn.Callback = @interactiveOnClick;

    function interactiveOnClick(~, ~)
        tabgroup.SelectedTab = interactiveTab;
        inputs = getProcessImageInputs(inputsTabObj);
        init_interactive_tab(interactiveTab, inputsTabObj.image, inputs.pixelScale, inputs.imageFileName);
    end

    function backFromInputsBtnCallback(~, ~)
        tabgroup.SelectedTab = startTab;
    end

    function nextFromInputsBtnCallback(~, ~)
        tabgroup.SelectedTab = plotsTab;
        init_plots_tab();
    end

inputsTabObj.nextBtn.Callback = @nextFromInputsBtnCallback;
inputsTabObj.backBtn.Callback = @backFromInputsBtnCallback;


% Plots tab
plotsTabObj = [];
    function init_plots_tab()
        plotsTabObj = plots_tab(plotsTab, getProcessImageInputs(inputsTabObj), inputsTabObj.imageProcessingResults, inputsTabObj.image);
        plotsTabObj.backBtn.Callback = @backFromPlotsCallback;
        plotsTabObj.nextBtn.Callback = @nextFromPlotsCallback;
        plotsTabObj.globalFftBtn.Callback = @globalFftCallback;
        plotsTabObj.localFftBtn.Callback = @localFftCallback;
    end

    function backFromPlotsCallback(~, ~)
        tabgroup.SelectedTab = imageTab;
    end

    function globalFftCallback(~, ~)
        tabgroup.SelectedTab = fftTab;
        init_fft_tab(fftTab, inputsTabObj.image,...
            inputsTabObj.imageProcessingResults, true);
    end

    function localFftCallback(~, ~)
        tabgroup.SelectedTab = fftTab;
        init_fft_tab(fftTab, inputsTabObj.image,...
            inputsTabObj.imageProcessingResults, false);
    end

    function nextFromPlotsCallback(~, ~)
        %         if (shouldGoToFFT(plotsTabObj))
        %             tabgroup.SelectedTab = fftTab;
        %             init_fft_tab(fftTab, inputsTabObj.image,...
        %                 inputsTabObj.imageProcessingResults);
        %         else
        tabgroup.SelectedTab = saveTab;
        init_save_tab(saveTab, getProcessImageInputs(inputsTabObj));
        %         end
    end


%Save settings tab
nextFromSaveBtn = uicontrol('Parent', saveTab,...
    'Units', 'Normalized',...
    'Position', [.85, .05, .1, .05],...
    'Style', 'pushbutton',...
    'String', 'Production',...
    'FontSize', 10,...
    'Callback', @nextFromSaveCallback);

    function nextFromSaveCallback(~, ~)
        tabgroup.SelectedTab = productionTab;
    end

backFromSaveBtn = uicontrol('Parent', saveTab,...
    'Units', 'Normalized',...
    'Position', [.05, .05, .1, .05],...
    'Style', 'pushbutton',...
    'String', 'Back',...
    'FontSize', 10,...
    'Callback', @backFromSaveCallback);

    function backFromSaveCallback(~, ~)
        tabgroup.SelectedTab = plotsTab;
    end


%Interactive Tab

backFromInteractiveBtn = uicontrol('Parent', interactiveTab,...
    'Units', 'Normalized',...
    'Position', [.05, .05, .1, .05],...
    'Style', 'pushbutton',...
    'String', 'Back',...
    'FontSize', 10,...
    'Callback', @backFromInteractiveCallback);

    function backFromInteractiveCallback(~, ~)
        tabgroup.SelectedTab = imageTab;
        interactivePlotAxis.Visible = 'Off';
        interactivePlotAxis.Title.String = '';
        axes(interactivePlotAxis); cla; hold off
    end

%FFT tab

backFromFftBtn = uicontrol('Parent', fftTab,...
    'Units', 'Normalized',...
    'Position', [.05, .05, .1, .05],...
    'Style', 'pushbutton',...
    'String', 'Back',...
    'FontSize', 10,...
    'Callback', @backFromFftCallback);

    function backFromFftCallback(~, ~)
        clear global_tree
        cla(fftOriginalImageAxes);
        tabgroup.SelectedTab = plotsTab;
    end


%Production tab
init_production_tab(productionTab);
backFromProductionBtn = uicontrol('Parent', productionTab,...
    'Units', 'normalized',...
    'Position', [0.05, 0.05, 0.1, 0.05],...
    'Style', 'pushbutton',...
    'FontSize', 10,...
    'String', 'Back',...
    'Visible', 'on',...
    'Callback', @backFromProductionOnClick);

    function backFromProductionOnClick(~, ~)
        if straightToProduction == 1
            tabgroup.SelectedTab = startTab;
        else
            tabgroup.SelectedTab =  saveTab;
        end
    end

f.Units = 'normalized';
grateInfoPanel.Units = 'normalized';
f.Name = 'GRATE';
movegui(f, 'center');
f.Visible = 'on';
end
