%% Calculate the lattice spacings, given two peaks, image size and nm_to_pix
% conversion.
function d_spac = calc_latt_spac(peak1, peak2, im_size, pix_per_nm)
diff = peak1 - peak2;
dist = diff ./ im_size;
dist = 2/norm(dist,2);
d_spac = dist / pix_per_nm;
end