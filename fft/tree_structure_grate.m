% implementing tree data structure for quad_tree decomposition

function quad_tree = tree_structure_grate(image, plotAxes)
%% Creates and returns a tree structure for the image
% addpath(genpath('/home/balajip/Documents/MATLAB/tinevez-matlab-tree-3d13d15'))

pix_per_nm = 1;
% [imageFileName, foldername]  = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.img; *.IMG; *.tif; *.TIF; *.tiff, *.TIFF'},...
%   'Select image to interact with');
% addpath(foldername);

I = image;
% check for rgb:
if length(size(I))==3
    I = rgb2gray(I);
end
% check for non-grayscale image
if max(I(:)) > 1
    % convert into grayscale
    I = mat2gray(I);
end

max_depth = 3;    % max depth of tree
quad_tree = tree();
num_splits = 4;
info.ifSplit = true;
% xmin, xmax, ymin, ymax of the global image (root node)
info.ranges = [1, size(I,1), 1, size(I,2)];


% update fft information on root node
info.fft_info = fft_calculation(I, pix_per_nm);
info.noOfPeaks = info.fft_info.noOfPeaks;

quad_tree = quad_tree.addnode(0, info);
plot_leaves(quad_tree, I, plotAxes);

curr_leaves = quad_tree.findleaves();
prev_leaves = [];

curr_depth = 0;
nPeaks = zeros(1, num_splits);    % gather of number of peaks in the children
% curr_depth = curr_depth + 1;
plot_leaves(quad_tree, I, plotAxes);
while (curr_depth < max_depth) && (numel(curr_leaves) ~= numel(prev_leaves))
    
    % while curr_depth < max_depth
    fprintf('Current depth of tree is: %d (out of %d)\n', curr_depth+1, max_depth);
    prev_leaves = curr_leaves;
    for leaf=curr_leaves
        % information about the current leaf image
        curr_info = quad_tree.get(leaf);
        if (curr_info.ifSplit)
            % split the image on the current leaf
            image_ranges = curr_info.ranges;
            sub_info = split_image(I(image_ranges(1):image_ranges(2), image_ranges(3): image_ranges(4)), image_ranges(1), image_ranges(3));
            % complete the fft_info for each child of the current leaf
            for i = 1:num_splits
                I_sub = I(sub_info(i).ranges(1):sub_info(i).ranges(2), sub_info(i).ranges(3):sub_info(i).ranges(4));
                sub_info(i).fft_info = fft_calculation(I_sub, pix_per_nm);
                sub_info(i).noOfPeaks = sub_info(i).fft_info.noOfPeaks;
                sub_info(i).ifSplit = calc_split_info(sub_info(i));
            end
            % add the children to the tree if they have any peaks in them. Otherwise,
            % do not add them to the tree.
            for ii = 1:num_splits
                nPeaks(ii) = sub_info(ii).noOfPeaks;
            end
            
            if (all(nPeaks == 0))
                for ii = 1:num_splits
                    sub_info(ii).ifSplit = false;
                end
            else
                for i = 1:num_splits
                    [quad_tree, ~] = quad_tree.addnode(leaf, sub_info(i));
                end
            end
            
        end
    end
    
    curr_depth = curr_depth + 1;
    curr_leaves = quad_tree.findleaves();
    plotAxes = plot_leaves(quad_tree, I, plotAxes);
end

% fprintf('Current depth of tree is: %d\n', curr_depth);
% save_leaves(quad_tree, pix_per_nm);
% plot_noOfPeaks_info(quad_tree);
% plot_blurred_image(quad_tree, I, plotAxes);
end

function [sub_info] = split_image(I, x0,y0)
%% divides the image I into 4 quadrants, I in the original image starts from
% x0,y0
% returns: sub_info, which is a structure of size 4 and contains the same
% information
% as in the structure in the global information structure
all_shape = size(I);
floor_halves = floor(all_shape/2);
x0 = x0-1;
y0 = y0-1;

% quadrant 1
sub_info(1).ranges = [x0+floor_halves(1), x0+all_shape(1) , y0+1, y0+floor_halves(2)];
sub_info(1).ifSplit = true;
sub_info(1).noOfPeaks = 100;

% quadrant 2
sub_info(2).ranges = [x0+1, x0+floor_halves(1) , y0+1, y0+floor_halves(2)];
sub_info(2).ifSplit = true;
sub_info(2).noOfPeaks = 100;
% quadrant 3
sub_info(3).ranges = [x0+1, x0+floor_halves(1) , y0+floor_halves(2), y0+all_shape(2)];
sub_info(3).ifSplit = true;
sub_info(3).noOfPeaks = 100;
% quadrant 4
sub_info(4).ranges = [x0+floor_halves(1), x0+all_shape(1) , y0+floor_halves(2), y0+all_shape(2)];
sub_info(4).ifSplit = true;
sub_info(4).noOfPeaks = 100;

end

function [fft_info] = fft_calculation(image, pix_per_nm)
%% fills in the fft_info structure on each image.
% Currently, it fills the following information:
%   noOfPeaks : no of pairs of peaks present in the fft
%   latt_spac : lattice spacings corresponding to those peaks
%   latt_orient : lattice orientations of the above peaks
% Care should be taken to not repeat the same orientation and spacing.

[F, F_bin] = fft_and_binarize(image);

CC=bwconncomp(F_bin);
stats = regionprops(CC,'Centroid');
% gather the centroids
fft_peaks = zeros(CC.NumObjects,2);
for jj = 1:CC.NumObjects
    fft_peaks(jj,1) = stats(jj).Centroid(1);
    fft_peaks(jj,2) = stats(jj).Centroid(2);
end
[center,fft_peaks] = find_and_remove_center(F,fft_peaks);

% finding pairs of peaks:
fft_pairs = [];
latt_orient = [];
latt_spac = [];
im_size = size(image);
if size(fft_peaks,1)
    % finding pairs of peaks _only_ if there are peaks other than the center peak:
    % this is done be calculating the reflection of the original set of peaks
    % and comparing with the original set
    [fft_pairs,fft_peaks] = make_pairs(fft_peaks,center);
    latt_orient = zeros(size(fft_peaks,1),1);
    latt_spac = zeros(size(fft_peaks,1),1);
    for jj = 1:size(fft_pairs,1)
        % calculating actual lattice spacings based on size : in nm
        dist_x_y = (fft_peaks(fft_pairs(jj,1),:) - fft_peaks(fft_pairs(jj,2),:));
        
        % lattice orientations
        latt_orient(jj) = atand(dist_x_y(2)./dist_x_y(1));
        
        % lattice spacings for the above pairs in inverse pixels. In order to use
        % these for plotting, they need to be converted into nm/pixels for proper
        % plots
        dist_x_y = dist_x_y ./ im_size;
        latt_spac(jj) = 2/norm(dist_x_y,2);
        latt_spac(jj) = latt_spac(jj) / pix_per_nm;
    end
end

fft_info.Centroid = center;          % should also be able to get from info.ranges!
fft_info.noOfPeaks = size(fft_peaks,1);
fft_info.peaks = fft_peaks;
fft_info.pairs = fft_pairs;
fft_info.latt_spac = latt_spac;
fft_info.latt_orient = latt_orient;

% if the mean pixel value in the image is at the extremes of the gray scale,
% then do not split the image.

% if mean(image(:)) < 0.45 || mean(image(:)) > 0.55
%   % extreme case scenario
%   fft_info.noOfPeaks = 0;
%   fft_info.peaks = [];
%   fft_info.pairs = [];
%   fft_info.latt_spac = [];
%   fft_info.latt_orient = [];
% end


end

function [F,F_bin] = fft_and_binarize(image)
%% performs FFT and binarization operation.
% the binarized fft will be used to calculate further information like the
% d-spacings and orientations take fft and do necessary shifts.
I = image;
F = fft2(I);
F = fftshift(F);        % Center FFT
F = abs(F);             % Get the magnitude
F = log(F+1);           % Use log, for perceptual scaling, and +1 since log(0) is undefined
x = (1:size(F,2))-round(size(F,2)/2);
y = (1:size(F,1))-round(size(F,1)/2);
[X,Y] = meshgrid(x,y);
[fitresult, ~] = createFit(X,Y,F);        % calculates using fitting, the best fit surface
% fit equation: z = a + c*exp(-((x.^2) /d1 + (y.^2)/d2 ))
a = fitresult.a;
c = fitresult.c;
d1 = fitresult.d1;
d2 = fitresult.d2;
sub_surf= a + c * exp( -( (X.^2)./(d1) + (Y.^2)./(d2)));    % surface to be subtracted from the filtered FFT
F_sub_fit = F - sub_surf;                                   % fft that has the fitted surface subtracted from it
F_sub_fit = exp(F_sub_fit);                                 % bring back the fft into non-scaled values
F_positive = double(F_sub_fit > 2.8*std(F_sub_fit(:))) ;      % part of cleaned fft that is more than 2 std deviations
f_cle = bwmorph(F_positive,'clean');                        % cleans single pixels

F_bin = bwmorph(f_cle,'thicken',3);                         % thickens the pixels so that nearby components are joined
% to neglect the peaks that come at the end of FFT
F_bin(2:end-1, 1) = 0;
F_bin(2:end-1, end) = 0;
F_bin(1, 2:end-1) = 0;
F_bin(end, 2:end-1) = 0;
end

function ifSplit = calc_split_info(image_info)
%% analyses the fft information and image size and decides whether to
% split the image or not.
if (image_info.noOfPeaks >= 1)
    ifSplit = true;
else
    ifSplit = false;
end

end

function [center,fft_peaks] = find_and_remove_center(F,fft_peaks)
%% finds and removes the center peak from the identified fft_peaks.
% If no center peak exists, it will simply return the fft_peaks as is.
center = size(F')/2;
peak_loc = -1;
for i = 1:size(fft_peaks,1)
    if(norm(center - fft_peaks(i,:)) < 3)
        peak_loc = i;
    end
end
if (peak_loc ~= -1)
    fft_peaks(peak_loc,:) = [];
end

end

function [pairs,fft_peaks] = make_pairs(fft_peaks,center)
%% Makes pairs from sets of coordinates in fft_peaks.
% recalculates peak locations if pairs are not formed.

A = fft_peaks;
len_peaks = size(fft_peaks,1);
fft_reflection = fft_peaks - 2 * (fft_peaks - repmat(center,[len_peaks,1]));
B = fft_reflection;
pairs = [];
if(len_peaks ==1)
    fft_peaks = [fft_peaks;fft_reflection];
    pairs = [1,2; 2,1];
else
    for i = 1:len_peaks             % iterate over identified peaks
        for j = 1:len_peaks           % iterate over reflections of identified peaks
            
            if (A(i,1) == 1000000000 || B(j,1) == 1000000000)
                % neglect the peaks already paired - we are checking only one
                % coordinate but that should be sufficient to eliminate the already
                % paired peaks
                continue;
            end
            
            if(norm(A(i,:) - B(j,:) ) <= 5+eps)
                % the reflection and original peaks are now paired
                pairs = [pairs;     i , j];
                % dummy values to not consider those peaks for further consideration
                A(i,:) = 1000000000;
                B(j,:) = 1000000000;
            end
        end
    end
end
if (size(pairs,1) < len_peaks)
    % append unpaired peaks into fft_peaks and remake the pairs
    fft_peaks = [fft_peaks;fft_reflection(A(:,1)~=1e9,:)];
    [pairs,fft_peaks] = make_pairs(fft_peaks,center);
end
% now fft_peaks(pairs(:,1)) and fft_peaks(pairs(:,2)) are the respective
% pairs
end

function plotAxes = plot_leaves(mytree, I, plotAxes)
%% plots the leaves of the decomposition tree on an image
axes(plotAxes);
% imshow(I, [], 'InitialMagnification','fit')
axis equal
hold on
colorOrder = ...
    [ 0 0 1 % 1 BLUE
    0 1 0 % 2 GREEN (pale)
    1 0 0 % 3 RED
    0 1 1 % 4 CYAN
    1 0.5 0.25 % 5 ORANGE
    1 1 0 % 6 YELLOW (pale)
    0.7 0.7 0.7 % 7 GREY
    0 0.75 0.75 % 8 TURQUOISE
    0 0.5 0 % 9 GREEN (dark)
    0.75 0.75 0 % 10 YELLOW (dark)
    1 0 1 % 11 MAGENTA (pale)
    0.75 0 0.75 % 12 MAGENTA (dark)
    0 0 0 % 13 BLACK
    0.8 0.7 0.6 % 14 BROWN (pale)
    0.6 0.5 0.4 ]; % 15 BROWN (dark)

dt = mytree.depthtree;
c = mytree.findleaves;
for i=c
    d(1:4) = mytree.get(i).ranges;
    plot([d(3), d(4), d(4), d(3), d(3)], [d(1), d(1), d(2), d(2),d(1)],...
        'LineWidth', 2.4, 'Color', colorOrder(dt.get(i)+1,:));
    hold on;
    drawnow();
end
end

function plot_noOfPeaks_info(mytree)
%% plots a tree that has the number of peaks as the node data
b = mytree.depthtree;
for i = 1:b.nnodes
    b = b.set(i,mytree.get(i).noOfPeaks/2);
end
figure(150); b.plot;
end


function save_leaves(mytree, pix_per_nm)
%% Save the information in the leaves, like in interactive FFT
dt = mytree.depthtree;
dt1 = dt;
for i = 1:dt1.nnodes
    dt1 = dt1.set(i,mytree.get(i).noOfPeaks/2);
end

c = mytree.findleaves;

% start writing into the file
fileID = fopen('FFT_info_qtree.txt','a+');
fprintf(fileID, 'location_x(pix),  location_y(pix),   Area(nm^2),   latt_spac,   latt_orient\n');

for i=c
    if (dt1.get(i) > 0)    % leaves with peaks
        try
            fft_info = reduce_data(mytree.get(i).fft_info);
            latt_spac = fft_info.latt_spac;
            latt_orient = fft_info.latt_orient;
            noOfPeaks = fft_info.noOfPeaks;
            ranges = mytree.get(i).ranges;
            % start writing for every peak observed
            for jj = 1:noOfPeaks
                % TODO: check the thresholds on the latt spacings
                if (latt_spac(jj) > 1.0) && (latt_spac(jj) < 4)
                    fprintf(fileID, '%12.f,  %12.f,   %12.2f,   %12.2f,   %12.2f     \n', ...
                        0.5*(ranges(1)+ranges(2)), 0.5*(ranges(3) + ranges(4)), ...
                        (ranges(2)-ranges(1))*(ranges(4)-ranges(3))/(pix_per_nm^2), ...
                        latt_spac(jj), latt_orient(jj));
                end
            end
        catch
            sprintf('Error in writing information of leaf %d', i);
        end
    end
end
fclose(fileID);
end

function resized_data = reduce_data(data)
%% remove repetition of peaks and pairs from the data.
% finds the unique values of latt_spac and latt_orient as well as the
% corresponding peak locations
resized_data = data;
[~, ind] = reduce_my_pairs_data(data.pairs);
resized_data.pairs = data.pairs(ind, :);
resized_data.noOfPeaks = length(ind);
resized_data.latt_orient = data.latt_orient(ind,:);
resized_data.Centroid = data.Centroid;
resized_data.latt_spac = data.latt_spac(ind);
end
