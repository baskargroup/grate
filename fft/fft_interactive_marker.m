%% FFT_INTERACTIVE_MARKER
% This is a standalone application that can be used by the used to get an
% estimate of the lattice spacings and orientations in the image. This should be
% used as a starting point for determining delta_r and delta_theta in the final
% analysis.

clear
close all
%% Selecting an image from the folder.
image = uigetfile('*.*','Select and image to analyze in this folder');

thres2 = 5; % thresold for the thresholded and cleaned fft
% thresholding fft using cftool instead of histogram of pixel values. 
%%

I_orig = imread(image);
ifContinueFFT = true;
I = I_orig;


iter = 0;       % number of fft iterations / sub-image selections

while(ifContinueFFT)
    
    %% marker selection of image
    if(iter==0)
        fig_original = figure;
        imshow(I_orig,'InitialMagnification','fit'); axis square
        h = imrect;
        ax_original = gca;
        position=wait(h);
    else
        figure(fig_original);
        h = imrect(ax_original,getPosition(h));
        addNewPositionCallback(h,@(p) title(sprintf('Selection area:%s',mat2str(p,3))));
        fcn = makeConstrainToRectFcn('imrect',get(ax_original,'XLim'),get(ax_original,'YLim'));
        setPositionConstraintFcn(h,fcn);
        position=wait(h);
    end
    
    
    I = imcrop(I_orig,getPosition(h));                      % crop original image
    
    %% fft routines
    I_gf = imgaussfilt(I);
    I = imresize(I_gf,1);
    
    % take fft and do necessary shifts
    F = fft2(I);
    F = fftshift(F);        % Center FFT
    F = abs(F);             % Get the magnitude
    F = log(F+1);           % Use log, for perceptual scaling, and +1 since log(0) is undefined
    F_filtered = F;
    %%
    % TODO: downsample the curve fit tool so that it is faster
    % FFT will be fitted using a curve fit. Then it will be thresholded to find the
    % location of the peaks.
    x = (1:size(F_filtered,2))-round(size(F_filtered,2)/2);
    y = (1:size(F_filtered,1))-round(size(F_filtered,1)/2);
    [X,Y] = meshgrid(x,y);
    [fitresult, gof] = createFit(X,Y,F_filtered);        % calculates using fitting, the best fit surface
    % fit equation: z = a + c*exp(-((x.^2) /d1 + (y.^2)/d2 ))
    % example result for coefficients after fit:    a = 9.084; c = 2.217; d1 = 1.5925e5; d2 = 8.557e4;
    a = fitresult.a;
    c = fitresult.c;
    d1 = fitresult.d1;
    d2 = fitresult.d2;
    sub_surf= a + c * exp( -( (X.^2)./(d1) + (Y.^2)./(d2)));    % surface to be subtracted from the filtered FFT
    F_sub_fit = F_filtered - sub_surf;                          % fft that has the fitted surface subtracted from it
    F_positive = double(F_sub_fit > 2*std(F_sub_fit(:))) ;      % part of cleaned fft that is more than 2 std deviations
    f_cle = bwmorph(F_positive,'clean');                        % cleans single pixels
    
    % The number of thicken iterations should be lower to not move the centroids
    % of the signal -- HEURISTIC
    f_processed = bwmorph(f_cle,'thicken',5);                % thickens the pixels so that nearby components are joined
    f_cle = f_processed;
    
    % finding the connected components of the thresholded fft.
    CC=bwconncomp(f_cle);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    
    % thres2 : a threshold used to determine whether the identified connected
    % components are actually signals or just noise. If the number of pixels in a
    % component is less than this value, then it is most probably noise. --
    % HEURISTIC
    thres2 = 3;
    
    for k = 1:length(numPixels)
        if(numPixels(k) < thres2) 
            % thres2 is based on the thicken strategy used.
            f_cle(CC.PixelIdxList{k}) = 0;
        end
    end
    
    CC=bwconncomp(f_cle);
    stats = regionprops(CC,'Centroid','MajorAxisLength','MinorAxisLength','Orientation','Area');
    fig_marked = figure;
    figure(fig_marked);
    imshow(F,[],'InitialMagnification','fit');
    
    hold on;
    datacursormode on
    
    display(sprintf('number of peaks identified = %d ',CC.NumObjects));
    for ii = 1:(CC.NumObjects)
        figure(fig_marked);
        plot(stats(ii).Centroid(1),stats(ii).Centroid(2),'*','MarkerSize',5,'MarkerFaceColor','blue','MarkerEdgeColor','green');
        plot(stats(ii).Centroid(1),stats(ii).Centroid(2),'o','MarkerSize',6,'MarkerFaceColor','red', 'MarkerEdgeColor','Green');
        display(sprintf('Centroid of peak %d(out of %d) is (%f,%f)',ii,CC.NumObjects,stats(ii).Centroid));
        drawnow;
        pause(0.5);
    end
    title('Identified peaks of the FFT');
    
    hold off;
    
    %% interactive input from user
    prompt = 'Do you want more FFTs on the image? Y/n [Y]: ';
    str = input(prompt,'s');
    if isempty(str)
        str = 'Y';
    end
    if(strcmpi(str,'y')==1)
        ifContinueFFT = true;
    elseif (strcmpi(str,'n')==1)
        ifContinueFFT = false;
    else
        error('Terminating FFT due to bad user input, defaulting to stopping the program!!')
    end
    
    iter = iter+1;
    
end