function [fft_peaks, fft_pairs, center]=fft_make_pairs(F, fft_peaks)

max_latt_spac = 500;
min_latt_spac = 10;


[center,fft_peaks] = find_and_remove_center(F,fft_peaks);
%% finding pairs of peaks:

if (mod(size(fft_peaks,1),2) ~= 0 )
    display('Odd number of peaks are found: others will be back-calculated');
    pause(2);
end

fft_pairs = [];
latt_spac = [];
latt_orient = [];

if size(fft_peaks,1)
    % finding pairs of peaks _only_ if there are peaks other than the center peak:
    % this is done be calculating the reflection of the original set of peaks
    % and comparing with the original set
    [fft_pairs,fft_peaks] = make_pairs(fft_peaks,center);
    dist_x_y = (fft_peaks(fft_pairs(:,1),:) - fft_peaks(fft_pairs(:,2),:));
    % lattice spacings for the above pairs
    latt_spac = sqrt(sum(dist_x_y.^2,2));
    % lattice orientations
    latt_orient = atand(dist_x_y(:,2)./dist_x_y(:,1));
    
    % filtering the lattice spacings based on physical feasibility
    [row_temp,~] = find(latt_spac < max_latt_spac & latt_spac > min_latt_spac);
    
    % Now, the centroids, lattics spacing and orientations can be accessed
    % by calling centroids(col(row_temp),:); latt_spac(col(row_temp));
    % latt_orient(col(row_temp)).
    %{
      figure('units','normalized','outerposition',[0 0 1 1]);
      imshow(F,[],'InitialMagnification','fit'); hold on;
      plot(fft_peaks((row_temp),1), fft_peaks((row_temp),2),'r*');
      axis square
      title('Filtered peaks of the original FFT, based on lattice spacing conditions');
    %}
    %if (size(fft_peaks,1) > 2  && center(1) > minSize/2)
    %ifSplit = true; % Don't split for interactive tab
    %else
    %       ifSplit = false;
    %     %end
    %   else
    %     % no peaks present in the block, except the center peak(which is
    %     % removed when find_and_remove_center is used)
    %     ifSplit = false;
    %   end
    %   if (ifSplit == false)
    %     % store information about the block only when splitting is terminated
    %     % for that block
    %     fft_ind = size(fft_data,2);
    %     fft_data(fft_ind+1).coord = (locate_part_of_image(I_resize,I_original));
    %     fft_data(fft_ind+1).Centroid = center;
    %     fft_data(fft_ind+1).noOfPeaks = size(fft_peaks,1);
    %     fft_data(fft_ind+1).peaks = fft_peaks;
    %     fft_data(fft_ind+1).pairs = fft_pairs;
    %     fft_data(fft_ind+1).latt_spac = latt_spac;
    %     fft_data(fft_ind+1).latt_orient = latt_orient;
    %     display(sprintf('Size of fft_data is : %d',size(fft_data,2)));
    %   end
end
end

function [center,fft_peaks] = find_and_remove_center(F,fft_peaks)
% finds and removes the center peak from the identified fft_peaks. If no
% center peak exists, it will simply return the fft_peaks as is.
center = size(F')/2;
peak_loc = -1;
for i = 1:size(fft_peaks,1)
    if(norm(center - fft_peaks(i,:)) < 3)
        peak_loc = i;
    end
end
if (peak_loc ~= -1)
    fft_peaks(peak_loc,:) = [];
end

end

function [pairs,fft_peaks] = make_pairs(fft_peaks,center)
% makes pairs from sets of coordinates in fft_peaks.
% recalculates peak locations if pairs are not formed.

A = fft_peaks;
len_peaks = size(fft_peaks,1);
fft_reflection = fft_peaks - 2 * (fft_peaks - repmat(center,[len_peaks,1]));
B = fft_reflection;
pairs = [];
if(len_peaks ==1)
    fft_peaks = [fft_peaks;fft_reflection];
    pairs = [1,2];
else
    for i = 1:len_peaks             % iterate over identified peaks
        for j = 1:len_peaks           % iterate over reflections of identified peaks
            
            if (A(i,1) == 1000000000 || B(j,1) == 1000000000)
                % neglect the peaks already paired - we are checking only one
                % coordinate but that should be sufficient to eliminate the already
                % paired peaks
                continue;
            end
            
            if(norm(A(i,:) - B(j,:) ) <= 5+eps)
                % the reflection and original peaks are now paired
                pairs = [pairs;     i , j];
                % dummy values to not consider those peaks for further consideration
                A(i,:) = 1000000000;
                B(j,:) = 1000000000;
            end
        end
    end
end
if (size(pairs,1) < len_peaks)
    display('Number of pairs less than the number of peaks!!')
    display('Reconstructing up the second peak by reflection about the center.');
    pause(0.5)
    % append unpaired peaks into fft_peaks and remake the pairs
    fft_peaks = [fft_peaks;fft_reflection(A(:,1)~=1e9,:)];
    [pairs,fft_peaks] = make_pairs(fft_peaks,center);
end
% now fft_peaks(pairs(:,1)) and fft_peaks(pairs(:,2)) are the respective
% pairs
end
