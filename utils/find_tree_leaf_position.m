%%
% Function to find the leaf that contains a given point
% Inputs: mytree: tree that contains the information
%             -- the (global) location of sub image is given in info.ranges 
% x0, y0: (global) location in the image
% Returns : leafId : ID of the leaf which contains the given point

function leafId = find_tree_leaf_position(mytree, x0, y0)

% Algorithm:
% Successively find that child who has the given point
leafId = 1;
childs = mytree.getchildren(leafId);
while length(childs) == 4
  % get children of this leaf
  is_inside = false(1, length(childs));
  for i = 1:length(childs)
    is_inside(i) = is_inside_leaf(mytree.get(childs(i)), x0, y0);
  end
  assert(sum(is_inside(:))==1)
  % update leafId and childs
  leafId = childs(boolean(is_inside));
  childs = mytree.getchildren(leafId);
end

end

function isInside = is_inside_leaf(info, x0, y0)
% Tells whether x0,y0 is inside the given leaf information
if (x0 > info.ranges(1) && x0 < info.ranges(2)) && (y0 > info.ranges(3) && y0 < info.ranges(4))
  isInside = true;
else
  isInside = false;
end
end