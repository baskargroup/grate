
classdef utils
    
    methods(Static)
        
        function [validated_value] = validate_input_range(value, min, max)
            if value < min
                validated_value = min;
            elseif value > max
                validated_value = max;
            else
                validated_value = value;
            end
        end
        
        function [newSubImage, rowsAdded, colsAdded] = pad_to_square(imageToPad)
            [r,c,~] = size(imageToPad);  %# Get the image dimensions
            nPad = abs(c-r);         %# The padding size

            if c > r                   %# Pad rows
                %I_resize = padarray(I,[floor(nPad) 0],...  %# Pad top
                %  0,'pre');
                newSubImage = padarray(imageToPad,[nPad 0],...   %# Pad bottom
                  0  ,'post');
                rowsAdded = nPad;
                colsAdded = 0;
            elseif r > c               %# Pad columns
                %I_resize = padarray(imageToPad,[0 floor(nPad)],...  %# Pad left
                %  0,'pre');
                newSubImage = padarray(imageToPad,[0 nPad],...   %# Pad right
                  0,'post');
                rowsAdded = 0;
                colsAdded = nPad;
            end
        end
        
        %Assumes input image is square
        function [newSubImage, rowsAdded, colsAdded] = makePow2Image(imageToPad)
            [r,~,~] = size(imageToPad);          %#r and c will be same
            nPad = abs(2^(nextpow2(r))-r) ;    %# The padding size
            %# Pad rows
            newSubImage = padarray(imageToPad,[floor(nPad) 0],...
                0,'post');
            %# Pad columns
            newSubImage = padarray(newSubImage,[0 floor(nPad)],...
                0,'post');

            rowsAdded = nPad;
            colsAdded = nPad;
        end
        
        function openHelpWindow(textToShow)
            helpdlg(textToShow)
        end
        
        function pixValue = convertnm2Pix(pixelScale, nmValue)
            pixValue = pixelScale*nmValue;
        end

        function nmValue = convertPix2nm(pixelScale, pixValue)
            nmValue = pixValue./pixelScale;
        end
    end
end