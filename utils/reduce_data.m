function resized_data = reduce_data(data)
% finds the unique values of latt_spac and latt_orient as well as the
% corresponding peak locations. look at reduce_my_pairs_data for more info
resized_data = data;
[~, ind] = reduce_my_pairs_data(data.pairs);
resized_data.pairs = data.pairs(ind, :);
resized_data.noOfPeaks = length(ind);
resized_data.latt_orient = data.latt_orient(ind,:);
resized_data.Centroid = data.Centroid;
resized_data.latt_spac = data.latt_spac(ind);
end
