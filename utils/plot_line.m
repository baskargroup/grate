function fig = plot_line(fig,corner,slope,siz)
figure(fig)
hold on;
center =  double(corner) + siz/2;
if(slope~=0)
  slope = slope + 90;
  for i = 1:size(slope,1)
    x = center(1) + siz/16  * [1 -1];
    y = center(2) + siz/16 * tand(slope(i)) * [1 -1];
    plot(x,y,'r','LineWidth',5);
  end
end
end