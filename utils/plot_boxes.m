function fig = plot_boxes(fig, data)
figure(fig);
hold on;
res_data = reduce_data(data); % take only the unique lattice spacings
if(res_data.noOfPeaks)
  % plot only if there are peaks
  for i = 1:res_data.noOfPeaks
    fig = plot_lines_of_box(fig, res_data);
    display(i);
  end
end
end

function fig = plot_lines_of_box(fig,data)
% plots the box around each block of qtdecomp
figure(fig)
hold on
siz = data.Centroid(1) *2 ;
plot(double(data.coord(2))+[0,siz,siz,0,0],double(data.coord(1))+[0,0,siz,siz,0],'b','LineWidth',5);
plot(double(data.coord(2))+[siz,0],double(data.coord(1))+[siz,0],'r','LineWidth',2);
plot(double(data.coord(2))+[siz,0],double(data.coord(1))+[0,siz],'r','LineWidth',2);
plot(data.coord(2),data.coord(1),'ro');
end