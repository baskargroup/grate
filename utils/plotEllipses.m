%% Copyright 2014-2016 Baskar Ganapathysubramanian
%% 
%% This file is part of GRATE.
%% 
%% GRATE is free software: you can redistribute it and/or modify it under the
%% terms of the GNU Lesser General Public License as published by the Free
%% Software Foundation, either version 2.1 of the License, or (at your option)
%% any later version.
%% 
%% GRATE is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
%% A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
%% details.
%% 
%% You should have received a copy of the GNU Lesser General Public License along
%% with GRATE.  If not, see <http://www.gnu.org/licenses/>.

% --- end license text --- %
%% Function to plot the specified points on an ellipse
% Plots the points on the ellipses specified by the variable phi

% Inputs:
% s : region properties of an image
% axisHandle : axis handle to plot the ellipse points on
% phi : angle in radians, will determine the points on the ellipse
% lineInfo : information about the line thickness and color

% Return variables:
% axes: axis handle for the final plot with the ellipses : this is
% essentially axisHandle + info of ellipses

function [axes] = plotEllipses(s,axisHandle,phi,lineInfo)
if(length(lineInfo)<4)
  error('Check the arguments that are passed');
end
cosphi = cos(phi);
sinphi = sin(phi);
for k = 1:length(s)
  %%
  xbar = s(k).Centroid(1);
  ybar = s(k).Centroid(2);
  
  a = s(k).MajorAxisLength/2;
  b = s(k).MinorAxisLength/2;
  
  theta = pi*s(k).Orientation/180;
  R = [ cos(theta)   sin(theta)
    -sin(theta)   cos(theta)];
  
  xy = [a*cosphi; b*sinphi];
  xy = R*xy;
  
  x = xy(1,:) + xbar;
  y = xy(2,:) + ybar;
  
  plot(axisHandle,x,y,lineInfo{1},lineInfo{2},lineInfo{3},lineInfo{4}); hold on;
end

axes = gca;
