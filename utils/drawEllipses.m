%% Copyright 2014-2016 Baskar Ganapathysubramanian
%% 
%% This file is part of GRATE.
%% 
%% GRATE is free software: you can redistribute it and/or modify it under the
%% terms of the GNU Lesser General Public License as published by the Free
%% Software Foundation, either version 2.1 of the License, or (at your option)
%% any later version.
%% 
%% GRATE is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
%% A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
%% details.
%% 
%% You should have received a copy of the GNU Lesser General Public License along
%% with GRATE.  If not, see <http://www.gnu.org/licenses/>.

% --- end license text --- %

%% Function that can plot the ellipses
% This is just a wrapper function for calling plotEllipses.m

% Inputs:
% s: properties of connected components
% axisHandle: plot in which the ellipses have to be plotted

% Return value:
% axes: plot with the ellipses on it.

function [axes]=drawEllipses(s,axisHandle,varargin)
%% set default plot parameters

if(length(varargin)<4)
	if(isempty(varargin))
		varargin{1} = 'LineWidth';
		varargin{2} = 2.5;
		varargin{3} = 'color';
		varargin{4} = 'magenta';
	elseif(length(varargin)==2)
		if strcmp(varargin{1},'LineWidth')
			varargin{3} = 'color';
			varargin{4} = 'magenta';
		elseif strcmp(varargin{1},'color')
			varargin{3} = 'LineWidth';
			varargin{4} = 2.5;
		end
	else
		error('Check the arguments for drawEllipses');
	end
end
varA = varargin;
%%
phi = linspace(0,2*pi,50);
axes = plotEllipses(s,axisHandle,phi,varA);
