function [reduced, reduced_indices] = reduce_my_pairs_data(repeated_pairs)
% removes the duplication of pairs of peaks in the pairs list.
% n : number of pairs
n = size(repeated_pairs, 1);
a = repeated_pairs(:,1);
b = repeated_pairs(:,2);
i = 0;
reduced = [];
reduced_indices = [];
while length(a) > n/2
  i = i + 1;
  j = 0;
  while length(b) > n/2
    j = j+1;
    if a(i) == b(j)
      a(j) = [];
      b(j) = [];
      j = j - 1;
      reduced = [reduced; a(i), b(i)];
      reduced_indices = [reduced_indices ; find(repeated_pairs(:,1) == a(i))];
      break
    end
  end
end
reduced = unique(reduced, 'rows');
end